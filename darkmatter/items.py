# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

# from .utils import clean_string, clean_number, clean_text, clean_float, clean_int
from urlparse import urljoin


class DarkmatterItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    agency_id = scrapy.Field()
    checksum = scrapy.Field()
    image_stored_urls = scrapy.Field()
    images = scrapy.Field()
    object_id = scrapy.Field()
    spider_name = scrapy.Field()
    object_type = scrapy.Field()

    type = scrapy.Field()
    title = scrapy.Field()
    archived = scrapy.Field()
    bathrooms = scrapy.Field()
    bedrooms = scrapy.Field()
    description = scrapy.Field()
    image_urls = scrapy.Field()
    latitude = scrapy.Field()
    longitude = scrapy.Field()
    price = scrapy.Field()
    price_type = scrapy.Field()
    rooms = scrapy.Field()
    square = scrapy.Field()
    address = scrapy.Field()
    url = scrapy.Field()

    zip = scrapy.Field()

    def get_image_urls(self):
        return [urljoin(self['url'], image_url) for image_url in self.get('image_urls', [])]
