import pymongo
from pymongo.mongo_client import MongoClient

client = MongoClient('localhost', 27017)  # default maxPoolSize = 100
db = client['darkmatter']
#
# item_collection = db['item']
# item_collection.create_index([
#     ('name', pymongo.ASCENDING),
# ], unique=True)