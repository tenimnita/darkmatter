# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo

from darkmatter import my_mongo


class DumpPipeline(object):
    def process_item(self, item, spider):
        my_mongo.db[spider.name].create_index([
            ('url', pymongo.ASCENDING),
        ], unique=True)
        my_mongo.db[spider.name].insert_one(dict(item))
        spider.logger.info('saved')
        return item
