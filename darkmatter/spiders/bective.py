# -*- coding: utf-8 -*-

import re
from functools import partial

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem
from urlparse import urljoin


class BectiveSpider(scrapy.Spider):
    name = "bective"
    allowed_domains = ["bective.co.uk"]

    start_urls = [
        'http://bective.co.uk/properties-for-sale/london/page-all',
        'http://bective.co.uk/properties-to-rent/london/page-all',
        'http://bective.co.uk/properties-short-let/london/page-all',
        'http://bective.co.uk/land-and-new-homes/overview',
    ]

    FOR_SALE_REGEX = re.compile(r'^.*/(?:land-and-new-homes|propert(?:y|ies)-for-sale)/.*$')
    FOR_RENT_REGEX = re.compile(r'^.*/(?:propert(?:y|ies)-to-rent|propert(?:y|ies)-short-let|short-let-property-to-rent)/.*$')

    def __init__(self, *args, **kwargs):
        super(BectiveSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        item['title'] = response.css('#property-address *::text').extract()
        item['price'] = response.css('.prop_figures .prop_price::text').extract_first()
        item['bedrooms'] = response.css('.prop_figures .prop_beds::text').extract()
        item['description'] = response.css('#weknowproperties::text').extract()

        #coordinates = extract_coordinates_for_google_maps(response)

        #if coordinates:
        #    item['latitude'] = coordinates[0]
        #    item['longitude'] = coordinates[1]

        image_urls = response.css('#picture-image-overview .pic::attr(style)').re(r'url\([\'"](.+)[\'"]\)')
        image_urls = ["/" + url for url in image_urls if not url.startswith('/')]

        item['image_urls'] = image_urls

        if self.FOR_SALE_REGEX.match(response.url):
            item['type'] = 'sale'
        elif self.FOR_RENT_REGEX.match(response.url):
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)

        if item['type'] == 'rent':
            if response.css('.prop_figures .prop_price *::text').re(re.compile(r'pw', flags=re.IGNORECASE)):
                item['price_type'] = 'week'
            else:
                self.logger.warning('Unknown price type for the estate: %s', item['url'])

        yield item

    def parse(self, response):
        for item_link in response.css('.result-box .inner .street a'):
            url = item_link.css('a::attr(href)').extract()

            if url:
                url = urljoin(response.url, item_link.css('a::attr(href)')[0].extract())
                yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(url=url)))
            else:
                self.logger.warning('Cannot extract URL from: %s', item_link.extract())
