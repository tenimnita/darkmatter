# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HarpersandharrisonSpider(scrapy.Spider):
    name = "harpersandharrison"
    allowed_domains = ["www.harpersandharrison.co.uk"]

    start_urls = [
        'http://www.harpersandharrison.co.uk/site/go/search?sales=true&country=GB',
        'http://www.harpersandharrison.co.uk/site/go/search?sales=false&country=GB'
    ]

    def __init__(self, *args, **kwargs):
        super(HarpersandharrisonSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        item['url'] = response.url
        item['title'] = response.xpath('//div[@id="particularsAddress"]/text()').extract_first()
        item['price'] = response.xpath('//div[@id="propertyPriceParticulars"]/text()').extract_first()

        if item['price']:
            matched = re.search(ur'£([\d,]+)\s(.*)', item['price'])
            if matched:
                item['price'] = matched.group(1)
                item['price_type'] = matched.group(2)

        item['description'] = response.xpath('//*[@id="pageParticulars"]/div[8]/p[1]//text()').extract()
        # # # Parse image urls
        image_urls = set()
        for url in response.xpath('//div[@id="thumbnails"]//img/@src').extract():
            matched = re.match('/photos/thumbnails/(\d+).jpg', url, re.IGNORECASE)
            if matched:
                image_urls.add('/photos/{}.jpg'.format(matched.group(1)))
        image_urls.add(response.xpath('//div[@id="pageParticulars"]/img/@src').extract_first())
        image_urls = [urljoin(base=response.url, url=url) for url in image_urls]
        image_urls = list(image_urls)
        item['image_urls'] = image_urls

        bed_room_text = response.xpath('//div[@id="propertyTypeParticulars"]/text()').extract_first()
        if bed_room_text:
            matched = re.match('(\d+) bedroom', bed_room_text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        if u'sales=true' in response.url:
            type = 'sale'
        else:
            type = 'rent'
        urls = response.xpath('//div[@id="propertyAddress"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract item URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//td[@class="pageList"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
