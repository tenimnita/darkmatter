# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class TemplateSpider(scrapy.Spider):
    name = "harpertweedie"
    allowed_domains = ["www.harpertweedie.com"]

    start_urls = [
        'http://www.harpertweedie.com/properties/sales',
        'http://www.harpertweedie.com/properties/lettings'
    ]

    FOR_SALE_REGEX = re.compile(r'.*/lettings')
    FOR_RENT_REGEX = re.compile(r'.*/sales')

    def __init__(self, *args, **kwargs):
        super(TemplateSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h3/text()').extract_first()
        item['price'] = response.xpath(
            '//div[@class="container secondary-toggle"]/div[@class="col-lg-5"]/h3/text()').extract_first()
        item['description'] = response.xpath('//div[@id="togglable_home"]/p//text()').extract()
        # # # # Parse image urls
        image_urls = response.xpath('//div[@id="slider"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath('//div[@class="container secondary-toggle"]//h4/text()').extract_first()

        if self.FOR_SALE_REGEX.match(response.url):
            item['type'] = 'sale'
        elif self.FOR_RENT_REGEX.match(response.url):
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="property-small"]/div/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//ul[@class="top list-nav"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
