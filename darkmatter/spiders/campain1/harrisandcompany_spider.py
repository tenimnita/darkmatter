# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HarrisandcompanySpider(scrapy.Spider):
    name = "harrisandcompany"
    allowed_domains = ["www.harrisandcompany.co.uk"]

    start_urls = [
        'http://www.harrisandcompany.co.uk/search.aspx?listingtype=5&category=1',
        'http://www.harrisandcompany.co.uk/search.aspx?listingtype=6&category=1',
    ]

    def __init__(self, *args, **kwargs):
        super(HarrisandcompanySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/span/text()').extract_first()
        item['price'] = response.xpath('//h3/span/text()').extract_first()
        item['description'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblBriefDescription"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//ul[@class="ad-thumb-list"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblBedrooms"]/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblBathrooms"]/text()').extract_first()

        if u'/for-sale/' in response.url:
            item['type'] = 'sale'
        elif u'/for-rent/' in response.url:
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//a[@class="propAdd"]/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
            # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
            #     paging_url = urljoin(base=response.url, url=paging_url)
            #     yield Request(url=paging_url, callback=self.parse)
