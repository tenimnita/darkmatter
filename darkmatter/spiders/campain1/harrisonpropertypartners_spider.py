# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class TemplateSpider(scrapy.Spider):
    name = "harrisonpropertypartners"
    allowed_domains = ["www.harrisonpropertypartners.com"]

    start_urls = [
        'http://www.harrisonpropertypartners.com/Search?listingType=5&areainformation=&radius=&minprice=&maxprice=&bedrooms=&cipea=1&statusids=1&obc=Price&obd=Descending&page=1',
        'http://www.harrisonpropertypartners.com/Search?listingType=6&areainformation=&radius=&minprice=&maxprice=&bedrooms=&cipea=1&statusids=1&obc=Price&obd=Descending&page=1'
    ]

    def __init__(self, *args, **kwargs):
        super(TemplateSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//h2/div/text()').extract_first()
        item['description'] = response.xpath(
            '//div[@class="row fullDescription"]//em[contains(text(), "Description")]/following-sibling::text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//div[@id="property-photos-device1"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        if u'/for-sale/' in response.url:
            item['type'] = 'sale'
        elif u'/for-rent/' in response.url:
            item['type'] = 'rent'
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="row propertyRow"]//h2/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        if len(urls) > 0:
            matched = re.match(r'(.*)&page=(\d+)$', response.url)
            if matched:
                yield Request(url=u'{}&page={}'.format(matched.group(1), int(matched.group(2)) + 1),
                              callback=self.parse)
            else:
                self.logger.warning(u'no match paging {}'.format(response.url))
