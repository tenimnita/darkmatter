# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HarrisonspropertySpider(scrapy.Spider):
    name = "harrisonsproperty"
    allowed_domains = ["www.harrisonsproperty.co.uk"]

    start_urls = [
        'http://www.harrisonsproperty.co.uk/search/1.html?n=8&showstc=on&showsold=on&orderby=price+asc&instruction_type=Sale&property_type[]=',
        'http://www.harrisonsproperty.co.uk/search/?instruction_type=Letting&ShowSTC=on&ShowSold=on&address_keyword=&bedrooms=&property_type=',
    ]

    def __init__(self, *args, **kwargs):
        super(HarrisonspropertySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h3/text()').extract_first()
        if item['title']:
            matched = re.match(ur'(.*)\s(£.*)\s+\|\s+(.*)', item['title'])
            if matched:
                item['title'] = matched.group(3)
                item['price'] = matched.group(2)

        item['description'] = response.xpath('//div[@id="propertyDetails"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@id="pikachoose"]//img/@src').extract()
        image_urls = [urljoin(base=response.url, url=url) for url in image_urls]
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="propertyLinks1"]/a/@href').extract()
        type = None
        if u'instruction_type=Letting' in response.url:
            type = 'rent'
        elif u'instruction_type=Sale' in response.url:
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            url = self.remove_query(url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//div[@class="resultsPages"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)

    def remove_query(self, url):
        u = urlparse(url)
        u = u._replace(query=None)
        return u.geturl()
