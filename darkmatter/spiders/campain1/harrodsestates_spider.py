# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HarrodsestatesSpider(scrapy.Spider):
    name = "harrodsestates"
    allowed_domains = ["www.harrodsestates.com", 'properties.harrodsestates.com']

    start_urls = [
        'http://properties.harrodsestates.com/search?w=&af=tab:tolet',
        'http://properties.harrodsestates.com/search?w=&af=tab:tobuy'
    ]

    def __init__(self, *args, **kwargs):
        super(HarrodsestatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//div[@class="guideprice"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="propertybox"]/p//text()').extract()
        # # # Parse image urls
        item['image_urls'] = response.xpath('//a[@class="fancybox"]/@href').extract()
        if u'/lettings/' in response.url:
            item['type'] = 'rent'
        elif u'/sales/' in response.url:
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="propertysummary"]/h2/a/@title').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        else:
            for url in urls:
                url = urljoin(response.url, url)
                yield Request(url=url, callback=self.parse_item)
            for paging_url in response.xpath('//li[@class="sli_page_link"]/a/@href').extract():
                paging_url = urljoin(base=response.url, url=paging_url)
                yield Request(url=paging_url, callback=self.parse)
