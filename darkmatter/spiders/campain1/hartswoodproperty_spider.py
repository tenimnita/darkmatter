# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HartswoodpropertySpider(scrapy.Spider):
    name = "hartswoodproperty"
    allowed_domains = ["www.hartswoodproperty.co.uk"]

    start_urls = [
        'http://www.hartswoodproperty.co.uk/php/proplist.php',
    ]

    def __init__(self, *args, **kwargs):
        super(HartswoodpropertySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//p[@class="Address"]/text()').extract_first()
        item['price'] = response.xpath('//p[@class="Rental"]/text()').extract_first()
        item['description'] = response.xpath('//p[@class="BigDesc"]//text()').extract()
        # # # Parse image urls
        image_urls = [urljoin(response.url, url) for url in response.xpath('//ul[@id="slideshow"]//img/@src').extract()]
        item['image_urls'] = set()
        for url in image_urls:
            matched = re.match(r'http://www\.hartswoodproperty\.co\.uk/proman/(.*)T.(.*)', url)
            if matched:
                item['image_urls'].add(
                    u'http://www.hartswoodproperty.co.uk/proman/{}W.{}'.format(matched.group(1), matched.group(2)))
            else:
                item['image_urls'].add(url)
        item['image_urls'] = list(item['image_urls'])
        item['type'] = 'rent'
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//tr[@id="proptablerow"]//a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            if url.startswith(u'./propinfo.php'):
                url = urljoin(response.url, url)
                yield Request(url=url, callback=self.parse_item)
