# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HarveyresidentialSpider(scrapy.Spider):
    name = "harveyresidential"
    allowed_domains = ["harveyresidential.com"]

    start_urls = [
        'http://harveyresidential.com/?s=&post_type=listing&lookingto=rent',
        'http://harveyresidential.com/?s=&post_type=listing&lookingto=buy'
    ]

    REGEX_RENT = re.compile(r'(.*)&lookingto=rent$')
    REGEX_SALE = re.compile(r'(.*)&lookingto=buy')

    def __init__(self, *args, **kwargs):
        super(HarveyresidentialSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//b[text()="Price:"]/following-sibling::text()').extract_first()
        item['description'] = response.xpath('//div[@class="entry-content"]/p//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//div[@class="entry-content"]/p/img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath('//b[text()="Bedrooms:"]/following-sibling::text()').extract_first()
        item['bathrooms'] = response.xpath('//b[text()="Bathrooms:"]/following-sibling::text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="listing-wrap"]/a/@href').extract()
        type = None
        if self.REGEX_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            # print url
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//li[@class="pagination-next"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            # print paging_url
            yield Request(url=paging_url, callback=self.parse)
