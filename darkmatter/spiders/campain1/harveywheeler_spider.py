# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HarveywheelerSpider(scrapy.Spider):
    name = "harveywheeler"
    allowed_domains = ["www.harveywheeler.com"]

    start_urls = [
        'http://www.harveywheeler.com/?s=&post_type=listing&price=&bedrooms=&status=',
    ]

    def __init__(self, *args, **kwargs):
        super(HarveywheelerSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//h2/text()').extract_first()
        item['description'] = response.xpath('//div[@class="entry-content"]/p[1]//text()').extract()
        # # # # Parse image urls
        item['image_urls'] = set()
        for img_url in response.xpath('//ul[@class="slides"]/li/img/@src').extract():
            matched = re.match(r'(.*)-150x150\.(.*)', img_url)
            if matched:
                item['image_urls'].add(u'{}.{}'.format(matched.group(1), matched.group(2)))
        image_urls = list(item['image_urls'])
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//b[text()="Bedrooms:"]/following-sibling::text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//b[text()="Bathrooms:"]/following-sibling::text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="listing-wrap"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            # print url
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//li[@class="pagination-next"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            # print paging_url
            yield Request(url=paging_url, callback=self.parse)
