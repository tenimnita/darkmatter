# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HastingsinternationalSpider(scrapy.Spider):
    name = "hastingsinternational"
    allowed_domains = ["www.hastingsinternational.com"]

    start_urls = [
        'http://www.hastingsinternational.com/flats-in-all-areas/for-sale/Any-Any/within-this-area-only/Any',
        'http://www.hastingsinternational.com/flats-in-all-areas/to-let/Any-Any/within-this-area-only/Any',
        'http://www.hastingsinternational.com/flats-in-all-areas/short-lets/Any-Any/within-this-area-only/Any'
    ]

    REGEX_FOR_RENT = re.compile(r'.*-for-rent/.*')
    REGEX_FOR_SALE = re.compile(r'.*-for-sale/.*')

    def __init__(self, *args, **kwargs):
        super(HastingsinternationalSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblFormattedAddress"]/text()').extract_first()
        item['price'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblFormattedPrice"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="property-fulldetails-section"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//ul[@class="ad-thumb-list"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblBedrooms"]/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblBathrooms"]/text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        if self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="ListResultContainer"]/h3/div/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            # print url
            yield Request(url=url, callback=self.parse_item)
            # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
            #     paging_url = urljoin(base=response.url, url=paging_url)
            #     print paging_url
            # yield Request(url=paging_url, callback=self.parse)
