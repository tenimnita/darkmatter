# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HathawaysSpider(scrapy.Spider):
    name = "hathaways"
    allowed_domains = ["www.hathaways.co.uk"]

    start_urls = [
        'http://www.hathaways.co.uk/westminster-properties/for-sale/period-houses/default.aspx',
        'http://www.hathaways.co.uk/westminster-properties/for-sale/flat-sales/default.aspx',
        'http://www.hathaways.co.uk/westminster-properties/for-sale/commercial/default.aspx',
        'http://www.hathaways.co.uk/westminster-properties/for-rent/studios/default.aspx',
        'http://www.hathaways.co.uk/westminster-properties/for-rent/one-bedroom/default.aspx',
        'http://www.hathaways.co.uk/westminster-properties/for-rent/two-bedroom/default.aspx',
        'http://www.hathaways.co.uk/westminster-properties/for-rent/three-bedroom/default.aspx',
        'http://www.hathaways.co.uk/westminster-properties/outside-sw1/for-sale/default.aspx'
    ]

    REGEX_FOR_SALE = re.compile(r'.*/for-sale/.*')
    REGEX_FOR_RENT = re.compile(r'.*/for-rent/.*')

    def __init__(self, *args, **kwargs):
        super(HathawaysSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//p[@class="title"]/text()').extract_first()
        item['price'] = response.xpath('//p[@class="title"]/text()[2]').extract_first()
        item['description'] = response.xpath('//div[@class="newdetail"]/p//text()').extract()
        if len(item['description']) == 0:
            item['description'] = response.xpath('//div[@class="description"]//text()').extract()
        # Parse image urls
        image_urls = [urljoin(base=response.url, url=url) for url in
                      response.xpath('//div[@u="slides"]//img[@u="image"]/@src').extract()]
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        if u'/one-bedroom/' in response.url:
            item['bedrooms'] = 1
        elif u'/two-bedroom/' in response.url:
            item['bedrooms'] = 2
        elif u'/three-bedroom/' in response.url:
            item['bedrooms'] = 3
        if self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="catalogeimg"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//div[@class="pages"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
