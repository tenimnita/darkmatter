# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CollierslondonSpider(scrapy.Spider):
    name = "hattonrealestate"
    # redirect to www.collierslondon.com
    allowed_domains = ["www.collierslondon.com"]

    SESSION = 'BAh7DEkiD3Nlc3Npb25faWQGOgZFVEkiRTA4MmJhNTVkYjZkMWZiOGE2NTY5%0AZjExM2ZjYTg0YzgwNThkNWFmYWY5Mjk0NGNkYTliNGYzMzM4MGZkMDc3NTcG%0AOwBGSSIUcmVjZW50bHlfdmlld2VkBjsARlsHewc6B2lkIh01MzRiZWQ4YjI1%0ANzQzMzAwMDIwMDAwMzE6D2RlcGFydG1lbnRJIg9Db21tZXJjaWFsBjsAVHsH%0AOwYiHTU3ZDAzYmVlNjFkZjZmMDAwOTAwMDAwNDsHSSIPQ29tbWVyY2lhbAY7%0AAFRJIhdjdXJyZW50X2RlcGFydG1lbnQGOwBGSSIPQ29tbWVyY2lhbAY7AFRJ%0AIhJjdXJyZW50X3VzYWdlBjsARkkiC3JldGFpbAY7AFRJIiVjdXJyZW50X3Nl%0AYXJjaF9wYXJhbXNfY29tbWVyY2lhbAY7AEZ7DEkiB3RvBjsAVEkiCExldAY7%0AAFRJIg9hcmVhX3F1ZXJ5BjsAVEkiAAY7AFRJIg1taW5fc2l6ZQY7AFRJIgAG%0AOwBUSSINbWF4X3NpemUGOwBUSSIABjsAVEkiCXNvcnQGOwBUSSISbGFyZ2Vz%0AdF9maXJzdAY7AFRJIglhcmVhBjsAVFsASSIOdXNlX2NsYXNzBjsAVFsASSIc%0AY3VycmVudF9zZWFyY2hfc2VsZWN0b3IGOwBGewhJIg5wdWJsaXNoZWQGOwBU%0AVEkiG2F2YWlsYWJsZV9ncm91cHNfY291bnQGOwBUewZJIggkZ3QGOwBUaQBJ%0AIgd0bwY7AFRAF0kiDHVzZXJfaWQGOwBGIh01N2Q0MjQ4N2NmZWQ1MTAwMGMw%0AMDAwNDg%3D%0A--aee2d2ad795f8cfbafdcf2f6f7fa268635e9ce24	';

    start_urls = [
        'http://www.collierslondon.com/Commercial?to=Let&area_query=&min_size=&max_size=&sort=largest_first',
    ]

    REGEX_FOR_SALE = re.compile(r'For\sSale', re.IGNORECASE)
    REGEX_FOR_RENT = re.compile(r'for\sRent', re.IGNORECASE)

    def __init__(self, *args, **kwargs):
        super(CollierslondonSpider, self).__init__(*args, **kwargs)

    def make_requests_from_url(self, url):
        request = super(CollierslondonSpider, self).make_requests_from_url(url)
        request.cookies['CLR.session'] = self.SESSION
        return request

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        # print response.body
        item['url'] = response.url
        item['title'] = response.xpath('//h1//text()').extract()
        item['price'] = response.xpath(
            '//ul[@class="basics sans-serif horizontal space-after-1"]/li[4]//text()').extract()
        item['description'] = response.xpath('//div[@class="L-1-2 S-1-1  gutters space-after-3"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="trunk"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        item['square'] = response.xpath(
            '//ul[@class="basics sans-serif horizontal space-after-1"]/li[3]//text()').extract()
        item['type'] = response.xpath(
            '//ul[@class="basics sans-serif horizontal space-after-1"]/li[1]//text()').extract()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        # print response.body
        urls = response.xpath('//div[@class="L-1-2 M-1-1"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
