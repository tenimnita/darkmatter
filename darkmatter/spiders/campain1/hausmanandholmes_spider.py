# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HathorpropertySpider(scrapy.Spider):
    name = "hausmanandholmes"
    allowed_domains = ["www.hausmanandholmes.com"]

    start_urls = [
        'http://www.hausmanandholmes.com/propertylisting?prevFilterBy=&hidDomain=sale',
        'http://www.hausmanandholmes.com/propertylisting?prevFilterBy=&hidDomain=let'
    ]

    REGEX_FOR_RENT = re.compile(r'.*&hidDomain=let')
    REGEX_FOR_SALE = re.compile(r'.*&hidDomain=sale')

    def __init__(self, *args, **kwargs):
        super(HathorpropertySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//span[@class="hd-top"]/text()').extract_first()
        item['price'] = response.xpath('//h2[@class="dtaddress"]/em/text()').extract_first()
        item['description'] = response.xpath('//div[@id="s-dtdesc"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//div[@id="owl-demo"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//ul[@class="rsbl- listbuttons"]/li[1]/a/@href').extract()
        type = None
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//a[@class="pg-nextbtn"]/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
