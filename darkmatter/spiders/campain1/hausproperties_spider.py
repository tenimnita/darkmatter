# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class TemplateSpider(scrapy.Spider):
    name = "hausproperties"
    allowed_domains = ["www.hausproperties.co.uk"]

    start_urls = [
        'http://www.hausproperties.co.uk/search/?showstc=on&instruction_type=Sale&address_keyword=&category=&bedrooms=0&minpricew=&maxpricew=&Search2=Search',
        'http://www.hausproperties.co.uk/search/?showstc=on&instruction_type=Letting&keyword=&property_type=&bedrooms=0&minpricew=&maxpricew=&Search2=Search'
    ]

    REGEX_FOR_SALE = re.compile(r'.*&instruction_type=Sale')
    REGEX_FOR_RENT = re.compile(r'.*&instruction_type=Letting')

    def __init__(self, *args, **kwargs):
        super(TemplateSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2[2]/br/following-sibling::text()').extract_first()
        item['price'] = response.xpath('//h2[2]/strong/text()').extract_first()
        item['description'] = response.xpath('//div[@class="panel-wrapper"]/div/p//text()').extract()
        # # # Parse image urls
        image_urls = set()
        img_regex = re.compile(r"'(/resize/(.*)/(\d+)/2000)'")
        for html in response.xpath('//script[@type="text/javascript"]//text()').extract():
            for t in html.split(','):
                matched = img_regex.search(t)
                if matched:
                    # print matched.group(1)
                    image_urls.add(urljoin(response.url, matched.group(1)))

        # image_urls = response.xpath('//ul[@class="slides"]//img/@src').extract()
        image_urls = list(image_urls)
        item['image_urls'] = image_urls
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="more-details"]/a/@href').extract()
        type = None
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//div[@class="pagination"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
