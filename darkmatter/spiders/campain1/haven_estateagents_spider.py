# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HavenEstateagentsSpider(scrapy.Spider):
    name = "haven-estateagents"
    allowed_domains = ["www.haven-estateagents.com"]

    start_urls = [
        'http://www.haven-estateagents.com/SearchResults.aspx?SearchData=v7|10|0|100000|0|99999998|0|0|3|3|1|1|1|Finchley|52649:19081:10:_|2|5|0|2|1||3||||416|0:2147483647:0|0|1|2|::',
        'http://www.haven-estateagents.com/SearchResults.aspx?SearchData=v7|10|0|100000|0|99999998|0|0|3|3|1|1|1|Finchley|52649:19081:10:_|2|5|0|2|1||3||||416|0:2147483647:0|0|2|2|::'
    ]

    def __init__(self, *args, **kwargs):
        super(HavenEstateagentsSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@id="property_info"]/h2//text()').extract()
        item['price'] = response.xpath('//span[@class="price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="property_details_description"]/p//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//ul[@id="property_details_gallery"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        # parse no. beds and baths
        text = response.xpath('//div[@id="property_info"]/p/text()').extract_first()
        # 4 beds, 2 baths
        matched = re.search(r'(\d+)\s+beds?', text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        matched = re.search(r'(\d+)\s+baths?', text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        # parse type
        text = response.xpath('//h4[@class="ico_magnify"]/text()').extract_first()
        if u'For Sale' in text:
            item['type'] = 'sale'
        elif u'For Rent' in text:
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//h5/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            # print url
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//fieldset[@id="searchDetails"]/p/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            # print paging_url
            yield Request(url=paging_url, callback=self.parse)
