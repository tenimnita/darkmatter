# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HawkandeagleSpider(scrapy.Spider):
    name = "hawkandeagle"
    allowed_domains = ["www.hawkandeagle.co.uk", "properties.hawkandeagle.co.uk"]

    PHPSESSID = 'dv36ube7sc6tqi80ed205o74t1'
    eaid = '242'

    start_urls = [
        # 'http://properties.hawkandeagle.co.uk/prop_description.php?propid=912',
        'http://properties.hawkandeagle.co.uk/index.php?tpages=86&adjacents=4&page=1',
        'http://properties.hawkandeagle.co.uk/index.php?tpages=6&adjacents=4&page=1'
    ]

    REGEX_FOR_SALE = re.compile(r'For\sSale', re.IGNORECASE)
    REGEX_FOR_RENT = re.compile(r'for\sRent', re.IGNORECASE)

    def __init__(self, *args, **kwargs):
        super(HawkandeagleSpider, self).__init__(*args, **kwargs)

    def make_requests_from_url(self, url):
        request = super(HawkandeagleSpider, self).make_requests_from_url(url)
        request.cookies['PHPSESSID'] = self.PHPSESSID
        request.cookies['eaid'] = self.eaid
        return request

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        # print response.body
        item['url'] = response.url
        item['title'] = response.xpath('//div[@id="propertyDescriptionLeft"]//text()').extract()
        item['price'] = response.xpath('//div[@id="propertyDescriptionRight"]/h2/text()').extract_first()
        item['description'] = response.xpath('//div[@class="freeholdcontent"]//text()').extract()
        # # # # Parse image urls
        image_urls = response.xpath('//ul[@class="ad-thumb-list"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        for text in item['title']:
            matched = self.REGEX_FOR_RENT.search(text)
            if matched:
                item['type'] = 'rent'
            matched = self.REGEX_FOR_SALE.search(text)
            if matched:
                item['type'] = 'sale'
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        # print response.body
        urls = response.xpath('//div[@class="dicritionpart"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            if not url.startswith('/'):
                url = '/' + url
            url = urljoin(response.url, url)
            # print url
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//div[@class="mainPaging"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            # print paging_url
            yield Request(url=paging_url, callback=self.parse)
