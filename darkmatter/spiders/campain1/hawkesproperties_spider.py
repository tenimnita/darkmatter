# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HawkespropertiesSpider(scrapy.Spider):
    name = "hawkesproperties"
    allowed_domains = ["www.hawkesproperties.com"]

    start_urls = [
        'http://www.hawkesproperties.com/property-search/?location=any&status=any&type=any&bedrooms=any&min-price=any&max-price=any&min-area&max-area',
    ]

    def __init__(self, *args, **kwargs):
        super(HawkespropertiesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@id="property-featured-image"]//img/@alt').extract_first()
        item['price'] = response.xpath('//h5[@class="price"]/span[2]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="content clearfix"]/p//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//ul[@class="slides"]/li/a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//i[@class="icon-bed"]/following-sibling::text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//i[@class="icon-bath"]/following-sibling::text()').extract_first()
        item['type'] = response.xpath('//span[@class="status-label"]/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//article/h4/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//div[@class="pagination"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
