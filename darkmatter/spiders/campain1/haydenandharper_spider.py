# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HaydenandharperSpider(scrapy.Spider):
    name = "haydenandharper"
    allowed_domains = ["www.haydenandharper.co.uk"]

    start_urls = [
        'http://www.haydenandharper.co.uk/search.aspx?ListingType=5&areainformation=&areainformationname=All+Locations&radius=0&statusids=1&igid=&imgid=&egid=&emgid=&category=1&defaultlistingtype=5&markettype=0&cur=GBP',
        'http://www.haydenandharper.co.uk/search.aspx?ListingType=6&areainformation=&areainformationname=All+Locations&radius=0&statusids=1&igid=&imgid=&egid=&emgid=&category=1&defaultlistingtype=5&markettype=0&cur=GBP'
    ]

    REGEX_FOR_RENT = re.compile(r'/for-rent/')
    REGEX_FOR_SALE = re.compile(r'/for-sale/')

    def __init__(self, *args, **kwargs):
        super(HaydenandharperSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//h3/span[1]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="BriefDescription"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//div[@id="gallery-1"]/img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        if self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        # item['bedrooms'] = response.xpath(
        #     '//img[@src="/images/bedrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['bathrooms'] = response.xpath(
        #     '//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="ListPropertyCellRight"]/h2/div/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
        #     paging_url = urljoin(base=response.url, url=paging_url)
        #     yield Request(url=paging_url, callback=self.parse)
