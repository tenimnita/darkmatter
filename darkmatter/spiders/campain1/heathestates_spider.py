# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HeathestatesSpider(scrapy.Spider):
    name = "heathestates"
    allowed_domains = ["www.heathestates.eu"]

    start_urls = [
        'http://www.heathestates.eu/results.asp',
    ]

    def __init__(self, *args, **kwargs):
        super(HeathestatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        # item['price'] = response.xpath('//small[@class="eapow-detail-price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="DetailText"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//div[@id="thumbs"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        # item['bedrooms'] = response.xpath('//img[@src="/images/bedrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['bathrooms'] = response.xpath('//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        for search_result in response.xpath('//table[@class="results1_propertyborder"]'):
            type = search_result.xpath('.//*[@class="results1_proptext_header"]/strong/span/text()').extract_first()
            bedroom = search_result.xpath('.//td[text()="BEDROOM"]/preceding-sibling::td/div/text()').extract_first()
            if bedroom is None:
                bedroom = search_result.xpath(
                    './/td[text()="BEDROOMS"]/preceding-sibling::td/div/text()').extract_first()
            bathroom = search_result.xpath('.//td[text()="BATHROOM"]/preceding-sibling::td/div/text()').extract_first()
            if bathroom is None:
                bathroom = search_result.xpath(
                    './/td[text()="BATHROOMS"]/preceding-sibling::td/div/text()').extract_first()
            # print u'{} {} {}'.format(type, bedroom, bathroom)
            urls = search_result.xpath('.//div[@class="results1_proptext"]/a/@href').extract()
            for url in urls:
                if url.startswith('detail.asp'):
                    url = urljoin(response.url, url)
                    # print url
                    yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                        type=type,
                        bedrooms=bedroom,
                        bathrooms=bathroom
                    )))

        for paging_url in response.xpath('//td[@class="results_navTable"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
