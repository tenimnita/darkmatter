# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HeathgateSpider(scrapy.Spider):
    name = "heathgate"
    allowed_domains = ["www.heathgate.com"]

    start_urls = [
        'http://www.heathgate.com/?s=&search-submit=Search&status=sale&details_1=&details_2=&min=&max=&orderby=price&order=desc',
        'http://www.heathgate.com/?s=&status=rent&orderby=price&order=asc'
    ]

    REGEX_FOR_RENT = re.compile('.*&status=rent&')
    REGEX_FOR_SALE = re.compile('.*&status=sale&')

    def __init__(self, *args, **kwargs):
        super(HeathgateSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1[@class="entry-title"]/text()').extract_first()
        item['price'] = response.xpath('//span[@class="listing-price-value"]/text()').extract_first()
        item['price_type'] = response.xpath('//span[@class="listing-rental-period"]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="wpsight-listing-description-2"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@class="slides"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//span[text()="Bedrooms:"]/following-sibling::span/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//span[text()="Bathrooms:"]/following-sibling::span/text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="post-image listing-image alignnone"]//a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//ul[@class="page-numbers"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
