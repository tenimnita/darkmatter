# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class TemplateSpider(scrapy.Spider):
    name = "heatonproperty"
    allowed_domains = ["www.heatonproperty.com"]

    start_urls = [
        'http://www.heatonproperty.com/our-properties/',
    ]

    def __init__(self, *args, **kwargs):
        super(TemplateSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//*[@id="inner_content"]/div[1]/div/h5/span/text()').extract_first()
        item['price'] = response.xpath('//*[@id="inner_content"]/div[1]/div/h5/text()').extract_first()
        item['address'] = response.xpath('//*[@id="inner_content"]/div[1]/div/h6/text()').extract_first()
        item['description'] = response.xpath('//div[@id="property_description"]/p//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//div[@id="gallery"]/img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]

        if item['title'] is not None:
            matched = re.match(r'(\d+)\sbedroom', item['title'], re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
        item['type'] = 'rent'
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="property_info"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//a[@class="imgReplace arrow_right"]/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
