# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HeaverSpider(scrapy.Spider):
    name = "heaver"
    allowed_domains = ["www.heaver.net"]

    start_urls = [
        'http://www.heaver.net/search.php?salerent=sale&type=all&minprice=&maxprice=&minbeds=1&maxbeds=6&dosearch=1&x=70&y=17',
        'http://www.heaver.net/search.php?salerent=rental&type=all&minprice=&maxprice=&minbeds=1&maxbeds=6&dosearch=1&x=40&y=10'
    ]

    REGEX_FOR_SALE = re.compile('.*salerent=sale&')
    REGEX_FOR_RENT = re.compile('.*salerent=rental&')

    def __init__(self, *args, **kwargs):
        super(HeaverSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//p[@class="guideprice"]/text()').extract_first()
        item['description'] = response.xpath('//p[@class="desc"]//text()').extract()
        # Parse image urls
        image_urls = set()
        for img_url in response.xpath('//img[@class="bigimg"]/@src').extract():
            if not img_url.startswith('/'):
                img_url = u'/' + img_url
            image_urls.add(urljoin(response.url, img_url))
        image_urls = list(image_urls)
        item['image_urls'] = image_urls
        # item['bedrooms'] = response.xpath('//img[@src="/images/bedrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['bathrooms'] = response.xpath('//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//a[@class="fulldetails"]/@href').extract()
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        if len(urls) == 0:
            print response.body
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            if not url.startswith('/'):
                url = u'/' + url
            url = urljoin(response.url, url)
            # print url
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//a[@class="prevnext"]/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            # print paging_url
            yield Request(url=paging_url, callback=self.parse)
