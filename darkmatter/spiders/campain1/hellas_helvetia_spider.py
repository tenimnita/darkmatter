# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HellasHelvetiaSpider(scrapy.Spider):
    name = "hellas-helvetia"
    allowed_domains = ["www.hellas-helvetia.com"]

    start_urls = [
        'http://www.hellas-helvetia.com/SalesResults.aspx',
    ]

    def __init__(self, *args, **kwargs):
        super(HellasHelvetiaSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2[@class="dtaddress"]/span/text()').extract_first()
        item['price'] = response.xpath('//h2[@class="dtaddress"]/em/text()').extract_first()
        item['description'] = response.xpath('//div[@id="s-dtdesc"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//ul[@id="s-dtimglist"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]

        bedroom_text = response.xpath('//h3[@class="prop_description"]/text()').extract_first()
        matched = re.search(r'(\d)\sBedroom', bedroom_text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        item['type'] = 'sale'
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//li[@class="rsbldetails"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
