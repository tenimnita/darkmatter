# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HendonestatesSpider(scrapy.Spider):
    name = "hendonestates"
    allowed_domains = ["hendonestates.com"]

    start_urls = [
        'http://hendonestates.com/properties-search/?status=any&type=any&location=any&bedrooms=any&bathrooms=any&min-price=any&max-price=any',
    ]

    def __init__(self, *args, **kwargs):
        super(HendonestatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//span[@class="single-property-price price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="property-content"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//ul[@id="image-gallery"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath('//div[@class="single-property-wrapper"]//i[@class="meta-item-icon icon-bed"]/following-sibling::div/span[2]/text()').extract_first()
        item['bathrooms'] = response.xpath('//div[@class="single-property-wrapper"]//i[@class="meta-item-icon icon-bath"]/following-sibling::div/span[2]/text()').extract_first()
        item['type'] = response.xpath('//span[@class="meta-item-label" and text()="Status"]/following-sibling::span/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//article//a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//div[@class="pagination"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
