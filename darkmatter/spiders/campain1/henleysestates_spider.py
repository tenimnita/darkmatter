# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HenleysestatesSpider(scrapy.Spider):
    name = "henleysestates"
    allowed_domains = ["www.henleysestates.co.uk"]

    start_urls = [
        'http://www.henleysestates.co.uk/?id=34622&do=search&for=1&kwa%5B%5D=&type%5B%5D=&minprice=0&maxprice=99999999999&minbeds=0&Search=Search&id=34622&order=2&page=0&do=search',
        'http://www.henleysestates.co.uk/?id=34622&do=search&for=2&type%5B%5D=&kwa%5B%5D=&Search=Search&id=34622&order=2&page=0&do=search&minprice=0&maxprice=99999999999&minbeds=0'
    ]

    REGEX_FOR_RENT = re.compile('.*&for=2&')
    REGEX_FOR_SALE = re.compile('.*&for=1&')

    def __init__(self, *args, **kwargs):
        super(HenleysestatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self,item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        # item['url'] = response.url
        # item['title'] = response.xpath('//h1/text()').extract_first()
        # item['price'] = response.xpath('//small[@class="eapow-detail-price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="text-wrapper"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//div[@id="galleria"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//span[text()="bedroom(s)"]//preceding-sibling::text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//div[@style="background-image:url(../../media/henleysestates.co.uk/images/default-icons-small/bath.png)"]/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        for item_selector in response.xpath('//div[@class="results-loop"]'):
            url = item_selector.xpath('.//input[@value="View Details"]/@onclick').extract_first()
            price = item_selector.xpath('.//h3/strong/text()').extract_first()
            title = item_selector.xpath('.//h3/text()').extract()

            matched = re.search(r"window\.location='(.*)'", url)
            if matched:
                url = urljoin(base=response.url, url=matched.group(1))
                # print u'{} | {} | {}'.format(url, price, title)
                yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                    type=type,
                    url=url,
                    price=price,
                    title=title
                )))
