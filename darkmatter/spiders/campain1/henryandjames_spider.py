# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HenryandjamesSpider(scrapy.Spider):
    name = "henryandjames"
    allowed_domains = ["www.henryandjames.co.uk"]

    start_urls = [
        'http://www.henryandjames.co.uk/search?type=sales&location=&min=&max=&beds=&keywords=',
        'http://www.henryandjames.co.uk/search?type=lettings&location=&min=&max=&beds=&furnished=Any&keywords=',
    ]

    IMG_REGEX = re.compile('(.*)/thumbs/(.*)')
    REGEX_FOR_RENT = re.compile('.*/search\?type=letting')
    REGEX_FOR_SALE = re.compile('.*/search\?type=sale')

    def __init__(self, *args, **kwargs):
        super(HenryandjamesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@class="propertyname"]/h1/text()').extract_first()
        item['price'] = response.xpath('//div[@class="bigprice"]/text()').extract_first()
        item['description'] = response.xpath('//*[@id="propertydetails"]/div[6]/div[2]//text()').extract()
        # # # Parse image urls
        image_urls = set()
        for url in response.xpath('//div[@id="slide-pager"]//img/@src').extract():
            matched = self.IMG_REGEX.match(url)
            if matched:
                image_urls.add(u'{}/{}'.format(matched.group(1), matched.group(2)))
        image_urls = list(image_urls)
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]

        for text in response.xpath('//*[@id="propertydetails"]/div[6]/ul[1]//text()').extract():
            matched = re.match(r'(\d+) Bedrooms?', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
            matched = re.match(r'(\d+) Bathrooms?', text, re.IGNORECASE)
            if matched:
                item['bathrooms'] = matched.group(1)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="thumbimage"]/a/@href').extract()
        type = None
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//span[@class="nextbutton"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
