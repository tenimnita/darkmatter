# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HenrywiltshireSpider(scrapy.Spider):
    name = "henrywiltshire"
    allowed_domains = ["www.henrywiltshire.co.uk"]

    start_urls = [
        'http://www.henrywiltshire.co.uk/buy-london-property/',
        'http://www.henrywiltshire.co.uk/london-properties-to-rent/'
    ]

    REGEX_FOR_RENT = re.compile('.*-rent/')
    REGEX_FOR_SALE = re.compile('.*/buy-')

    IMG_REGEX = re.compile('background-image:url\((.*)\)')

    def __init__(self, *args, **kwargs):
        super(HenrywiltshireSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//div[@id="propfeaturegriditem"]//h3/text()').extract_first()
        item['description'] = response.xpath('//section[@class="propertydesc"]//text()').extract()
        # # # Parse image urls
        image_urls = set()
        for img_url in response.xpath('//div[@class="rsBgimg"]/@style').extract():
            matched = self.IMG_REGEX.match(img_url)
            if matched:
                image_urls.add(matched.group(1))
        item['image_urls'] = list(image_urls)

        item['bedrooms'] = response.xpath('//li[@class="bedrooms"]/span//text()').extract_first()
        item['bathrooms'] = response.xpath('//li[@class="bathrooms"]/span//text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//h4[@class="title"]/a/@href').extract()
        type = None
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
