# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HetheringtonsSpider(scrapy.Spider):
    name = "hetheringtons"
    allowed_domains = ["www.hetheringtons.co.uk"]

    start_urls = [
        'http://www.hetheringtons.co.uk/Buy/Search/Theydon%20Bois/within-30-miles/',
        'http://www.hetheringtons.co.uk/Buy/Search/CM16/within-30-miles/',
        'http://www.hetheringtons.co.uk/rent/search/CM16/within-30-miles/',
        'http://www.hetheringtons.co.uk/rent/search/Theydon%20Bois%20Underground%20Station%20(Essex)/within-30-miles/'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/rent/.*')
    REGEX_FOR_SALE = re.compile(r'.*/buy/.*')

    def __init__(self, *args, **kwargs):
        super(HetheringtonsSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//label[@class="generic-price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="One"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@id="DetailsThumbnailsContainer"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = []
        IMG_REGEX = re.compile('(.*)/3.jpg', re.IGNORECASE)
        for img_url in image_urls:
            matched = IMG_REGEX.match(img_url)
            if matched:
                item['image_urls'].append(u'{}/11.jpg'.format(matched.group(1)))

        item['bedrooms'] = response.xpath(
            '//div[@class="l-property-header t-property-header"]//h3/text()').extract_first()
        # item['bathrooms'] = response.xpath(
        #     '//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="Two"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            # print url
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//ul[@id="ResultsListPagingTop"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
