# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HeywoodsSpider(scrapy.Spider):
    name = "heywoods"
    allowed_domains = ["www.heywoods.net"]

    start_urls = [
        'http://www.heywoods.net/Search?listingType=5&areainformation=&radius=&minprice=&maxprice=&bedrooms=&statusids=4%2C3&statusids=1&obc=Price&obd=Ascending&page=1',
        'http://www.heywoods.net/Search?listingType=6&areainformation=&radius=&minprice=&maxprice=&bedrooms=&statusids=6%2C3&statusids=1&obc=Price&obd=Ascending'
    ]

    REGEX_FOR_SALE = re.compile(r'.*/for-sale/')
    REGEX_FOR_RENT = re.compile(r'.*/for-rent/')

    def __init__(self, *args, **kwargs):
        super(HeywoodsSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2/text()').extract_first()
        item['price'] = response.xpath('//h3/div/text()').extract_first()
        item['description'] = response.xpath('//div[@class="row fdDescription"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//a[@class="rsImg"]/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        # item['bedrooms'] = response.xpath(
        #     '//img[@src="/images/bedrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['bathrooms'] = response.xpath(
        #     '//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//h2[@class="searchProName"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        if len(urls) > 0:
            matched = re.match(r'(.*)&page=(\d+)', response.url, re.IGNORECASE)
            if matched:
                yield Request(url=u'{}&page={}'.format(matched.group(1), int(matched.group(2)) + 1),
                              callback=self.parse)
            else:
                print 'no m'
