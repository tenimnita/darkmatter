# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HgpropertySpider(scrapy.Spider):
    name = "hgproperty"
    allowed_domains = ["www.hgproperty.co.uk"]

    start_urls = [
        'http://www.hgproperty.co.uk/?search-class=DB_CustomSearch_Widget-db_customsearch_widget&widget_number=preset-default&all-3=page&post_author-4=2&Category-7=2&search=Search&cs-Bedrooms-0=&cs-Price-1=&cs-Price-2=&cs-TypeID-5=&cs-AreaID-6=',
        'http://www.hgproperty.co.uk/?search-class=DB_CustomSearch_Widget-db_customsearch_widget&widget_number=preset-1&all-3=page&post_author-4=2&Category-7=1&search=Search&cs-Bedrooms-0=&cs-Price-1=&cs-Price-2=&cs-TypeID-5=&cs-AreaID-6='
    ]

    def __init__(self, *args, **kwargs):
        super(HgpropertySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@id="contentTitleBar"]/h2/span[1]/text()').extract_first()
        item['price'] = response.xpath('//div[@id="contentTitleBar"]/h2/span[2]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="fullDetails"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//div[@id="gallery"]//img/@src').extract()

        for text in response.xpath('//div[@id="vitalStats"]//text()').extract():
            matched = re.search(r'(\d)\sbathroom', text, re.IGNORECASE)
            if matched:
                item['bathrooms'] = matched.group(1)
            matched = re.search(r'(\d)\sbedroom', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
        breadcrumb = response.xpath('//div[@id="breadcrumb"]//a[2]/text()').extract_first()
        if u'All properties to let' in breadcrumb:
            item['type'] = 'rent'
        elif u'All properties for sale' in breadcrumb:
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="listingText"]//a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = self.remove_query(urljoin(response.url, url))
            # print url
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//div[@id="wp_page_numbers"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            # print paging_url
            yield Request(url=paging_url, callback=self.parse)

    def remove_query(self, url):
        tmp = urlparse(url)
        tmp = tmp._replace(query=None)
        return tmp.geturl()
