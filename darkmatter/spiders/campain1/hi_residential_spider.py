# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HiResidentialSpider(scrapy.Spider):
    name = "hi-residential"
    allowed_domains = ["hi-residential.com"]

    start_urls = [
        'http://hi-residential.com/search?type=buy&q=&propertyType=&minPrice=&maxPrice=&minBedrooms=&maxBedrooms=&view=&perPage=&sortBy=&polygon=',
        'http://hi-residential.com/search?type=rent&q=&propertyType=&minPrice=&maxPrice=&minBedrooms=&maxBedrooms=&view=&perPage=10&sortBy=price_high&polygon='
    ]

    def __init__(self, *args, **kwargs):
        super(HiResidentialSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath(
            '//div[@class="col-xs-18 search-result-property-display-address"]/text()').extract_first()
        item['price'] = response.xpath('//span[@class="search-result-property-price-value"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="col-xs-24 text-justify"]/p//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//a[@class="property-images-link"]/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//img[@class="bedroom-count"]/following-sibling::text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//img[@class="bathroom-count"]/following-sibling::text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="col-xs-24 search-result-property-display-address"]/a/@href').extract()
        type = None
        if u'type=buy' in response.url:
            type = 'sale'
        elif u'type=rent' in response.url:
            type = 'rent'
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//ul[@class="pagination"]/li/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
