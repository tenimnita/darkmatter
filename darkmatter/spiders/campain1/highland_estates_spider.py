# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HighlandEstatesSpider(scrapy.Spider):
    name = "highland-estates"
    allowed_domains = ["www.highland-estates.com"]

    start_urls = [
        'http://www.highland-estates.com/rent',
        'http://www.highland-estates.com/buy',
    ]

    def __init__(self, *args, **kwargs):
        super(HighlandEstatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//h2[@class="price"]').extract_first()
        item['description'] = response.xpath('//div[@id="description"]/p//text()').extract()
        # # # Parse image urls
        image_urls = set()
        for img_url in response.xpath('//div[@class="slidepicker"]//img/@src').extract():
            #     img/DSC08066.JPG/95/54
            matched = re.match(r'img/(.*)\.(.*)/(\d+)/(\d+)', img_url, re.IGNORECASE)
            if matched:
                image_urls.add(urljoin(base=response.url, url='/img/{}.{}'.format(matched.group(1), matched.group(2))))

        image_urls = list(image_urls)
        item['image_urls'] = image_urls
        item['type'] = response.xpath('//li[@class="current"]/a/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//ul[@class="listings"]/li/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//div[@id="pagination"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
