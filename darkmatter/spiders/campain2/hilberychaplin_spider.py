# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HilberychaplinSpider(scrapy.Spider):
    name = "hilberychaplin"
    allowed_domains = ["www.hilberychaplin.co.uk"]

    start_urls = [
        'http://www.hilberychaplin.co.uk/property-search/list?buy&resultpage=1',
        'http://www.hilberychaplin.co.uk/property-search/list?rent&resultpage=1',
    ]

    REGEX_FOR_RENT = re.compile(r'.*/properties-for-rent/')
    REGEX_FOR_SALE = re.compile(r'.*/properties-for-sale/')

    def __init__(self, *args, **kwargs):
        super(HilberychaplinSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//h2[@class="pfulldetails--price"]/text()').extract_first()
        item['description'] = response.xpath('//article[@id="property-description-content"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//nav[@class="pfulldetails--carousel-nav"]/img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        matched = re.match(r'(.*)&resultpage=(\d+)', response.url)
        if not matched:
            self.logger.warning(u'Not list page: {}'.format(response.url))
            return
        for item_node in response.xpath('//div[@class="psearch--content"]'):
            bed_rooms = item_node.xpath('./p[@class="bedrooms"]/text()').extract_first()
            url = item_node.xpath('./a[@class="psearch--button"]/@href').extract_first()
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(bedrooms=bed_rooms)))
        for paging_number in response.xpath(
                '//nav[@class="pagination-nav psearch--pagination"]/a/@data-resultpage').extract():
            yield Request(url=u'{}&resultpage={}'.format(matched.group(1), paging_number), callback=self.parse)
