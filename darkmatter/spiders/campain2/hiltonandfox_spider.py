# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HiltonandfoxSpider(scrapy.Spider):
    name = "hiltonandfox"
    allowed_domains = ["www.hiltonandfox.com"]

    start_urls = [
        'http://www.hiltonandfox.com/property_buyorrent/buy/',
        'http://www.hiltonandfox.com/property_buyorrent/commercial-2/',
        'http://www.hiltonandfox.com/property_buyorrent/rent/'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/rent/')
    REGEX_FOR_SALE = re.compile(r'.*/buy/')
    REGEX_FOR_COMMERCIAL = re.compile(r'.*/commercial-2/')

    def __init__(self, *args, **kwargs):
        super(HiltonandfoxSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2[@id="title"]/text()').extract_first()
        item['price'] = response.xpath('//h2[@id="pricebig"]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="listingcontent"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@class="slides"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        for text in response.xpath('//li[@class="four columns"]//text()').extract():
            matched = re.search(r'Bedrooms', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = text
            matched = re.search(r'Bathrooms', text, re.IGNORECASE)
            if matched:
                item['bathrooms'] = text
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="listingblocksection"]/a/@href').extract()
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_COMMERCIAL.match(response.url):
            type = 'commercial'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//div[@class="wp-pagenavi"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
