# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HklhomeSpider(scrapy.Spider):
    name = "hklhome"
    allowed_domains = ["www.hklhome.co.uk"]

    start_urls = [
        'http://www.hklhome.co.uk/search/1.html?showstc=on&showsold=off&instruction_type=Sale',
        'http://www.hklhome.co.uk/search/1.html?showstc=on&showsold=off&instruction_type=Letting'
    ]

    REGEX_FOR_SALE = re.compile(r'.*&instruction_type=Sale')
    REGEX_FOR_RENT = re.compile(r'.*&instruction_type=Letting')

    def __init__(self, *args, **kwargs):
        super(HklhomeSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/span/text()').extract_first()
        item['price'] = response.xpath('//span[@itemprop="price"]/text()').extract_first()
        item['description'] = response.xpath('//span[@itemprop="description"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@id="property-carousel"]/div[@class="carousel-inner"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        item['bedrooms'] = response.xpath(
            '//img[@alt="bedrooms"]/following-sibling::text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//img[@alt="bathrooms"]/following-sibling::text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="property"]//h3/a/@href').extract()
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//ul[@class="pagination"]/li/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
