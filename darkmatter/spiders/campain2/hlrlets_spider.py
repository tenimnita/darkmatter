# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HlrletsSpider(scrapy.Spider):
    name = "hlrlets"
    allowed_domains = ["www.hlrlets.com"]

    start_urls = [
        'http://www.hlrlets.com/let/property-to-let/',
    ]

    def __init__(self, *args, **kwargs):
        super(HlrletsSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//h3[@class="details_h3"]/span//text()').extract()
        item['description'] = response.xpath('//div[@id="propertyInfo_desc"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="tn3 album"]//li/a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        for text in response.xpath('//h3[@class="details_h3"]/text()').extract():
            matched = re.search('(\d+)\s+bedroom', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
            matched = re.search('(\d+)\s+bathroom', text, re.IGNORECASE)
            if matched:
                item['bathrooms'] = matched.group(1)
        item['type'] = 'rent'
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="results-list-details"]/h2/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//ul[@class="pagination"]/li/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
