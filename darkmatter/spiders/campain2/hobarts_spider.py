# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HobartsSpider(scrapy.Spider):
    name = "hobarts"
    allowed_domains = ["hobarts.co.uk"]

    start_urls = [
        'http://hobarts.co.uk/properties-for-sale/',
        'http://hobarts.co.uk/properties-to-rent/'
    ]

    REGEX_FOR_SALE = re.compile(r'.*\/properties-for-sale\/')
    REGEX_FOR_RENT = re.compile(r'.*\/properties-to-rent\/')

    def __init__(self, *args, **kwargs):
        super(HobartsSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//span[@class="details-price"]/text()').extract_first()
        item['description'] = response.xpath('//p[@class="property-description clearfix"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@id="lightSlider"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)

        for item_node in response.xpath('//ul[@class="property-listing"]/li'):
            bedrooms = item_node.xpath(
                './/p[@class="property-icons clearfix"]/label[1]/following-sibling::text()[1]').extract_first()
            bathrooms = item_node.xpath(
                './/p[@class="property-icons clearfix"]/label[3]/following-sibling::text()[1]').extract_first()
            url = item_node.xpath('.//a[@class="more-details"]/@href').extract_first()
            url = urljoin(response.url, url)
            # print u'{} {}'.format(bedrooms, bathrooms)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=type,
                bedrooms=bedrooms,
                bathrooms=bathrooms
            )))
        for paging_url in response.xpath('//p[@class="navigation"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
