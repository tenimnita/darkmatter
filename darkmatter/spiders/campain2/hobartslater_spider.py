# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HobartslaterSpider(scrapy.Spider):
    name = "hobartslater"
    allowed_domains = ["www.hobartslater.co.uk"]

    start_urls = [
        'http://www.hobartslater.co.uk/for-sale',
        'http://www.hobartslater.co.uk/for-rent'
    ]

    def __init__(self, *args, **kwargs):
        super(HobartslaterSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h3/text()').extract_first()
        item['price'] = response.xpath('//h4[@class="price"]//text()').extract()
        item['description'] = response.xpath('//div[@id="property-description"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@class="slides"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if u'/for-rent' in response.url:
            type = 'rent'
        elif u'/for-sale' in response.url:
            type = 'sale'
        for item_node in response.xpath('//div[@class="row portfolio-item status-0"]'):
            url = item_node.xpath('./div/a/@href').extract_first()
            url = urljoin(response.url, url)
            bedrooms = item_node.xpath('.//span[@class="bedrooms"]/text()').extract_first()
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=type,
                bedrooms=bedrooms
            )))
