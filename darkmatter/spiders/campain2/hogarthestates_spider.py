# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HogarthestatesSpider(scrapy.Spider):
    name = "hogarthestates"
    allowed_domains = ["www.hogarthestates.co.uk"]
    start_urls = [
        'http://www.hogarthestates.co.uk/buy/property-for-sale/?incsold=true',
        'http://www.hogarthestates.co.uk/let/property-to-let/?incsold=true'
    ]

    REGEX_FOR_SALE = re.compile(r'.*\/property-for-sale\/')
    REGEX_FOR_RENT = re.compile(r'.*\/property-to-let\/')

    def __init__(self, *args, **kwargs):
        super(HogarthestatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//span[@class="details_price"]//text()').extract()
        item['description'] = response.xpath('//div[@id="propertyInfo_desc"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="tn3 album"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        item_type = None
        if self.REGEX_FOR_RENT.match(response.url):
            item_type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item_type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        for item_node in response.xpath('//div[@class="results-gallery clearfix"]'):
            item = DarkmatterItem(type=item_type)
            url = item_node.xpath('.//h2[@class="results-gallery-address"]/a/@href').extract_first()
            url = urljoin(response.url, url)
            # item['url'] = url
            for text in item_node.xpath('.//div[@class="results-gallery-stats"]//text()').extract():
                matched = re.search(r'(\d+)\s+Bedroom', text, re.IGNORECASE)
                if matched:
                    item['bedrooms'] = matched.group(1)
                matched = re.search(r'(\d+)\s+Bathroom', text, re.IGNORECASE)
                if matched:
                    item['bathrooms'] = matched.group(1)
            yield Request(url=url, callback=partial(self.parse_item, item))
            for paging_url in response.xpath('//ul[@class="pagination"]/li/a/@href').extract():
                paging_url = urljoin(base=response.url, url=paging_url)
                yield Request(url=paging_url, callback=self.parse)
