# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HollandpropertiesSpider(scrapy.Spider):
    name = "hollandproperties"
    allowed_domains = ["hollandproperties.co.uk"]

    handle_httpstatus_list = [404]

    start_urls = [
        'http://hollandproperties.co.uk/wpsite/search.php?sales=false',
        'http://hollandproperties.co.uk/wpsite/search.php?sales=true'
    ]

    REGEX_FOR_SALE = re.compile(r'.*sales=true')
    REGEX_FOR_RENT = re.compile(r'.*sales=false')

    def __init__(self, *args, **kwargs):
        super(HollandpropertiesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        # item['title'] = response.xpath('//h4/text()').extract_first()
        # item['price'] = response.xpath('//small[@class="eapow-detail-price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="content clearfix"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@class="slides"]/li/a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//i[@class="icon-bed"]/following-sibling::text()').extract_first()
        # item['bathrooms'] = response.xpath(
        #     '//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        item_type = None
        if self.REGEX_FOR_RENT.match(response.url):
            item_type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item_type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        for item_node in response.xpath('//article'):
            title = item_node.xpath('./h4/a/text()').extract_first()
            price = item_node.xpath('.//h5[@class="price"]/text()').extract_first()
            url = item_node.xpath('./h4/a/@href').extract_first()
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=item_type,
                title=title,
                price=price
            )))
        # urls = response.xpath('//article/h4/a/@href').extract()
        # if len(urls) == 0:
        #     self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        # for url in urls:
        #     url = urljoinresponse.url, url)
        #     yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//td[@class="pageList"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
