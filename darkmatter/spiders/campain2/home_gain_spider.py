# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class TemplateSpider(scrapy.Spider):
    name = "home_gain"
    allowed_domains = ["www.home-gain.co.uk"]

    start_urls = [
        'http://www.home-gain.co.uk/property-search/',
    ]

    def __init__(self, *args, **kwargs):
        super(TemplateSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['description'] = response.xpath('//div[@class="content clearfix"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@class="slides"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        item['bedrooms'] = response.xpath('//div[@class="property-meta clearfix"]/span[1]/text()').extract_first()
        item['bathrooms'] = response.xpath('//div[@class="property-meta clearfix"]/span[2]/text()').extract_first()
        item['type'] = response.xpath('//span[@class="status-label"]/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        for item_node in response.xpath('//article[@class="property-item clearfix"]'):
            title = item_node.xpath('./h4/a/text()').extract_first()
            price = item_node.xpath('.//h5[@class="price"]/text()').extract_first()
            url = item_node.xpath('./h4/a/@href').extract_first()
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                title=title,
                price=price
            )))
        for paging_url in response.xpath('//div[@class="pagination"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
