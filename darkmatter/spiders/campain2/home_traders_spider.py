# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomeTradersSpider(scrapy.Spider):
    name = "home_traders"
    allowed_domains = ["www.home-traders.com"]

    start_urls = [
        'http://www.home-traders.com/properties/sales/sales-properties.php?s_offset=300',
        'http://www.home-traders.com/properties/lettings/lettings-properties.php?s_offset=300'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/lettings/')
    REGEX_FOR_SALE = re.compile(r'.*/sales/')

    def __init__(self, *args, **kwargs):
        super(HomeTradersSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h3/text()').extract_first()
        item['price'] = response.xpath('//li[@class="prop-price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="prop-detail"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@id="carousel-detail-page"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        # item['bedrooms'] = response.xpath(
        #     '//img[@src="/images/bedrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['bathrooms'] = response.xpath(
        #     '//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        for item_node in response.xpath('//div[@class="col-lg-12 prop-box"]'):
            item = DarkmatterItem()
            text = item_node.xpath('./div[2]/div[2]/text()').extract_first()
            matched = re.search(r'(\d+)\s+Bed', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
            onclick = item_node.xpath('.//ul[@class="prop-links"]/li[1]/a/@onclick').extract_first()
            matched = re.match(r'viewFunction\(propertyForm,\'(\d+)\',\'(\d+)\'\);', onclick)
            if matched:
                print matched.group(1)
                if u'/sales/' in response.url:
                    # item['type'] = 'sale'
                    yield Request(
                        url=u'http://www.home-traders.com/properties/sales/sales-property.php?property_id={}'.format(
                            matched.group(1)), callback=partial(self.parse_item, item))
                else:
                    # item['type'] = 'rent'
                    yield Request(
                        url=u'http://www.home-traders.com/properties/lettings/lettings-property.php?property_id={}'.format(
                            matched.group(1)), callback=partial(self.parse_item, item))
                    # for onclick in response.xpath('//ul[@class="prop-links"]/li[1]/a/@onclick').extract():
                    #     matched = re.match(r'viewFunction\(propertyForm,\'(\d+)\',\'(\d+)\'\);', onclick)
                    #     if matched:
                    #         print matched.group(1)
                    #         if u'/sales/' in response.url:
                    #             yield Request(
                    #                 url=u'http://www.home-traders.com/properties/sales/sales-property.php?property_id={}'.format(
                    #                     matched.group(1)), callback=self.parse_item)
                    #         else:
                    #             yield Request(
                    #                 url=u'http://www.home-traders.com/properties/lettings/lettings-property.php?property_id={}'.format(
                    #                     matched.group(1)), callback=self.parse_item)
