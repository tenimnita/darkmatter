# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomecareestatesSpider(scrapy.Spider):
    name = "homecareestates"
    allowed_domains = ["homecareestates.co.uk"]

    start_urls = [
        'http://homecareestates.co.uk/search?includeDisplayAddress=Yes&p_department=RS&location=',
        'http://homecareestates.co.uk/search?includeDisplayAddress=Yes&p_department=RL&location='
    ]

    REGEX_FOR_RENT = re.compile(r'.*/property-to-rent/')
    REGEX_FOR_SALE = re.compile(r'.*/property-for-sale/')

    def __init__(self, *args, **kwargs):
        super(HomecareestatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//span[@class="price"]/text()').extract_first()
        item['description'] = response.xpath('//p[@class="main_summary"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@id="property-details-slider"]/img[1]/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//span[@class="type"]/text()').extract_first()
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="search-results-gallery-property"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
        #     paging_url = urljoin(base=response.url, url=paging_url)
        #     yield Request(url=paging_url, callback=self.parse)
