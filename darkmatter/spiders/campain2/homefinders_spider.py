# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomefindersSpider(scrapy.Spider):
    name = "homefinders"
    allowed_domains = ["www.homefinders.net"]

    start_urls = [
        'http://www.homefinders.net/list-row-filter?field_branches_tid=All&field_type_tid=All&field_bedrooms_value=All&field_bathrooms_value=All&field_price_value%5Bmin%5D=100&field_price_value%5Bmax%5D=5000000&field_contract_type_tid%5B0%5D=45',
        'http://www.homefinders.net/list-row-filter?field_branches_tid=All&field_type_tid=All&field_bedrooms_value=All&field_bathrooms_value=All&field_price_value%5Bmin%5D=100&field_price_value%5Bmax%5D=5000000&field_contract_type_tid%5B0%5D=44',
    ]

    REGEX_FOR_RENT = re.compile(ur'.*&field_contract_type_tid%5B0%5D=45')
    REGEX_FOR_SALE = re.compile(ur'.*&field_contract_type_tid%5B0%5D=44')

    def __init__(self, *args, **kwargs):
        super(HomefindersSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//div[contains(@class, "field-name-field-price")]/div[2]/div/text()').extract_first()
        item['description'] = response.xpath('//div[@class="field-item even"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@id="pikachoose-field-images"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        # print response.url
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        for item_node in response.xpath('//div[@class="field-content"]'):
            url = item_node.xpath('.//div[@class="title span4"]/a/@href').extract_first()
            url = urljoin(response.url, url)
            price = item_node.xpath('.//div[@class="price span2"]/text()').extract_first()
            bedrooms = item_node.xpath('.//span[@class="bedrooms"]/text()').extract_first()
            bathrooms = item_node.xpath('.//span[@class="bathrooms"]/text()').extract_first()
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=type,
                # price=price,
                bedrooms=bedrooms,
                bathrooms=bathrooms
            )))
        for paging_url in response.xpath('//ul[@class="pager"]/li/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
