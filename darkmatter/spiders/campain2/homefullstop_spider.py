# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomefullstopSpider(scrapy.Spider):
    name = "homefullstop"
    allowed_domains = ["www.homefullstop.com"]

    start_urls = [
        'http://www.homefullstop.com/properties.aspx?Mode=0&PriceMax=0&Bedrooms=0&Areas=',
        'http://www.homefullstop.com/properties.aspx?Mode=1&PriceMax=0&Bedrooms=0&Areas='
    ]

    def __init__(self, *args, **kwargs):
        super(HomefullstopSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2/abbr/text()').extract_first()
        item['bedrooms'] = response.xpath('//div[@id="divMainPage"]/div/div[1]/h1/span/text()').extract_first()
        item['price'] = response.xpath('//div[@id="divMainPage"]/div/div[2]/h1/span/text()').extract_first()
        item['description'] = response.xpath('//div[@class="descriptionRight"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="tn3 album"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        # for text in response.xpath('//div[@class="descriptionRight"]/ul/li/text()').extract():
        #     matched = re.match(r'(\d+)\s+Bedroom', text, re.IGNORECASE)
        #     if matched:
        #         item['bedrooms'] = matched.group(1)
        #     matched = re.match(r'(\d+)\s+Bedroom', text, re.IGNORECASE)
        #     if matched:
        #         item['bedrooms'] = matched.group(1)
        if u'/property-for-sale' in response.url:
            item['type'] = 'sale'
        elif u'/property-to-rent' in response.url:
            item['type'] = 'rent'
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//a[text()="Click for Details"]/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
            # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
            #     paging_url = urljoin(base=response.url, url=paging_url)
            #     yield Request(url=paging_url, callback=self.parse)
