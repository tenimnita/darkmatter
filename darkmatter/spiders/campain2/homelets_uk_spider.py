# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomeletsUkSpider(scrapy.Spider):
    name = "homelets_uk"
    allowed_domains = ["www.homelets-uk.com"]

    start_urls = [
        'http://www.homelets-uk.com/property.php?ptype=to-let&location=&bedrooms=&price_min=0&price_max=0&submit=+',
    ]

    def __init__(self, *args, **kwargs):
        super(HomeletsUkSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@class="address"]/text()').extract_first()
        item['price'] = response.xpath('//span[@class="price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="fullDesc"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="photos"]/ul/li/a/@id').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        item['bedrooms'] = response.xpath(
            '//li[@class="bedroom"]/span/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//li[@class="bathroom"]/span/text()').extract_first()
        item['type'] = 'rent'
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="property_search"]//div[@class="image"]/div/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
        #     paging_url = urljoin(base=response.url, url=paging_url)
        #     yield Request(url=paging_url, callback=self.parse)
