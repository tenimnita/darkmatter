# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomelinkSpider(scrapy.Spider):
    name = "homelink"
    allowed_domains = ["www.homelink.co.uk"]

    start_urls = [
        'http://www.homelink.co.uk/let/property-to-let/minbedrooms/0/maxbedrooms/8/',
        'http://www.homelink.co.uk/buy/property-for-sale/minbedrooms/0/maxbedrooms/8/'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/property-to-let/')
    REGEX_FOR_SALE = re.compile(r'.*/property-for-sale/')

    def __init__(self, *args, **kwargs):
        super(HomelinkSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract()
        item['price'] = response.xpath('//h2[@class="property-details-price"]//text()').extract()
        item['description'] = response.xpath('//div[@class="large-7  columns property-details"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="lightboxcarousel"]/div/@data-image-src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        for text in response.xpath('//div[@class="row property-actions-area content-row"]//span/text()').extract():
            matched = re.search(r'(\d+)\s+bedroom', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
            matched = re.search(r'(\d+)\s+bathroom', text, re.IGNORECASE)
            if matched:
                item['bathrooms'] = matched.group(1)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        item_type = None
        if self.REGEX_FOR_RENT.match(response.url):
            item_type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item_type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="grid"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=item_type)))
        for paging_url in response.xpath('//ul[@class="pagination"]/li/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
