# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomemovepropertySpider(scrapy.Spider):
    name = "homemoveproperty"
    allowed_domains = ["homemoveproperty.co.uk"]

    start_urls = [
        'http://homemoveproperty.co.uk/properties/lettings/lettings-properties.php?s_trans_type=&s_min_price=&s_max_price=&s_min_bedrooms=&s_max_bedrooms=&s_search_type=&s_area=&property_code=',
        'http://homemoveproperty.co.uk/properties/sales/sales-properties.php?s_trans_type=2&s_min_price=&s_max_price=&s_min_bedrooms=&s_max_bedrooms=&s_search_type=&s_area=&property_code='
    ]

    REGEX_FOR_RENT = re.compile(r'.*/lettings/')
    REGEX_FOR_SALE = re.compile(r'.*/sales/')

    def __init__(self, *args, **kwargs):
        super(HomemovepropertySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//table[@class="topBand"]/tr/td[2]/text()').extract_first()
        item['price'] = response.xpath('//table[@class="topBand"]/tr/td[3]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="thumbnails"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//img[@class="imageBorder"]/@src').extract()
        image_urls = [urljoin(response.url, url) for url in image_urls]
        item['image_urls'] = image_urls
        # item['bedrooms'] = response.xpath(
        #     '//img[@src="/images/bedrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['bathrooms'] = response.xpath(
        #     '//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        item_type = None
        if self.REGEX_FOR_RENT.match(response.url):
            item_type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item_type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
            return
        for onclick in response.xpath('//td[@class="moreInfo"]/@onclick').extract():
            matched = re.match(r"viewFunction\(propertyForm,'(\d+)','(\d+)'\);", onclick)
            if matched:
                if item_type == 'rent':
                    url = u'http://homemoveproperty.co.uk/properties/lettings/lettings-property.php?property_id={}'.format(
                        matched.group(1))
                else:
                    url = u'http://homemoveproperty.co.uk/properties/sales/sales-property.php?property_id={}'.format(
                        matched.group(1))
                yield Request(url=url, callback=self.parse_item)
