# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class TemplateSpider(scrapy.Spider):
    name = "homesearchsales"
    allowed_domains = ["homesearchsales.co.uk"]

    start_urls = [
        'http://homesearchsales.co.uk/property-search/',
    ]

    def __init__(self, *args, **kwargs):
        super(TemplateSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/span/text()').extract_first()
        item['price'] = response.xpath('//h5[@class="price"]/span[2]/text()[1]').extract_first()
        item['description'] = response.xpath('//div[@class="content clearfix"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@class="slides"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        for text in response.xpath('//div[@class="property-meta clearfix"]/span/text()').extract():
            # print text
            # print '------------'
            matched = re.search(ur'(\d+)[\s\u00A0]+bedroom', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
            matched = re.search(ur'(\d+)[\s\u00A0]+bathroom', text, re.IGNORECASE)
            if matched:
                item['bathrooms'] = matched.group(1)
        item['type'] = response.xpath('//span[@class="status-label"]/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//article/h4/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
        #     paging_url = urljoin(base=response.url, url=paging_url)
        #     yield Request(url=paging_url, callback=self.parse)
