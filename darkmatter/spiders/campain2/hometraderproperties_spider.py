# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HometraderpropertiesSpider(scrapy.Spider):
    name = "hometraderproperties"
    allowed_domains = ["hometraderproperties.co.uk"]

    start_urls = [
        'https://hometraderproperties.co.uk/action/letting',
        'https://hometraderproperties.co.uk/action/sales',
    ]

    REGEX_FOR_RENT = re.compile(r'.*/action/letting')
    REGEX_FOR_SALE = re.compile(r'.*/action/sales')

    def __init__(self, *args, **kwargs):
        super(HometraderpropertiesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//span[@class="price_area"]//text()').extract()
        item['description'] = response.xpath('//div[@class="single-content listing-content"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="carousel-inner"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        item_type = None
        if self.REGEX_FOR_RENT.match(response.url):
            item_type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item_type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        for node_item in response.xpath('//div[@class="property_listing"]'):
            url = node_item.xpath('./h4/a/@href').extract_first()
            bedrooms = node_item.xpath('.//span[@class="inforoom"]/text()').extract_first()
            bathrooms = node_item.xpath('.//span[@class="infobath"]/text()').extract_first()
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=item_type,
                bedrooms=bedrooms,
                bathrooms=bathrooms
            )))
        for paging_url in response.xpath('//ul[@class="pagination pagination_nojax"]/li/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
