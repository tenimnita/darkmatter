# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomeviewestatesSpider(scrapy.Spider):
    name = "homeviewestates"
    allowed_domains = ["www.homeviewestates.com"]

    start_urls = [
        'http://www.homeviewestates.com/results.dtx?getdata=true&stype=rentals',
        'http://www.homeviewestates.com/results.dtx?stype=sales&getdata'
    ]

    REGEX_FOR_RENT = re.compile(r'.*stype=rentals')
    REGEX_FOR_SALE = re.compile(r'.*stype=sales')

    def __init__(self, *args, **kwargs):
        super(HomeviewestatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self,item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1[@class="details_h1"]/text()').extract_first()
        item['price'] = response.xpath('//h3[@class="details_h3"]/span/text()').extract_first()
        item['description'] = response.xpath('//div[@id="plongdescription_0"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="tn3 album"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        for text in response.xpath('//h3[@class="details_h3"]/text()').extract():
            matched = re.search(ur'(\d+)\s+bedroom', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
            matched = re.search(ur'(\d+)\s+bathroom', text, re.IGNORECASE)
            if matched:
                item['bathrooms'] = matched.group(1)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        item_type = None
        if self.REGEX_FOR_RENT.match(response.url):
            item_type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item_type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="results-list-morelink"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=item_type
            )))
        if len(urls) > 0:
            matched = re.match(r'(.*)&page=(\d+)', response.url)
            if matched:
                yield Request(url=u'{}&page={}'.format(matched.group(1), int(matched.group(2)) + 1),
                              callback=self.parse)
            else:
                yield Request(url=u'{}&page={}'.format(response.url, 2),
                              callback=self.parse)
