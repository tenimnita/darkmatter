# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomeviewresidentialSpider(scrapy.Spider):
    name = "homeviewresidential"
    allowed_domains = ["www.homeviewresidential.co.uk"]

    start_urls = [
        'http://www.homeviewresidential.co.uk/buy.asp?IDTSec=buy',
        'http://www.homeviewresidential.co.uk/rent.asp?IDTSec=rent'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/buy\.asp')
    REGEX_FOR_SALE = re.compile(r'.*/rent\.asp')

    def __init__(self, *args, **kwargs):
        super(HomeviewresidentialSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/a/text()').extract_first()
        item['price'] = response.xpath('//span[@class="details"]/text()').extract_first()
        item['description'] = response.xpath('//span[@class=""]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//a[@class="group1"]/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        if item['price']:
            matched = re.search('(\d+)\s+bedroom', item['price'], re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        item_type = None
        if self.REGEX_FOR_RENT.match(response.url):
            item_type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item_type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//h2/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=item_type
            )))
            # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
            #     paging_url = urljoin(base=response.url, url=paging_url)
            #     yield Request(url=paging_url, callback=self.parse)
