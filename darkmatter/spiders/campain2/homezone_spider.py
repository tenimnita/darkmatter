# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class HomezoneSpider(scrapy.Spider):
    name = "homezone"
    allowed_domains = ["www.vebra.com"]

    start_urls = [
        'http://www.vebra.com/Homezone2016/property/search/results/2/1',
        'http://www.vebra.com/Homezone2016/property/search/results/1/1'
    ]

    REGEX_FOR_SALE = re.compile('.*/search/results/1/.*')
    REGEX_FOR_RENT = re.compile('.*/search/results/2/.*')

    def __init__(self, *args, **kwargs):
        super(HomezoneSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2[@class="dtaddress"]/span/text()').extract_first()
        item['price'] = response.xpath('//h2[@class="dtaddress"]/em/text()').extract_first()
        item['description'] = response.xpath('//div[@id="s-dtdesc"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//ul[@id="s-dtimglist"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        text = response.xpath('//h3[@class="prop_description"]/text()').extract_first()
        matched = re.search(r'(\d+)\sBedroom', text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//li[@class="rsbldetails"]/a/@href').extract()
        type = None
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//ul[@class="s-pagenav"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
