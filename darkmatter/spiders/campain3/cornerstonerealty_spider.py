# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class TemplateSpider(scrapy.Spider):
    name = "cornerstonerealty"
    allowed_domains = ["www.cornerstonerealty.co.uk"]

    start_urls = [
        'http://www.cornerstonerealty.co.uk/residential/sales',
        'http://www.cornerstonerealty.co.uk/residential/rentals'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/rentals/')
    REGEX_FOR_SALE = re.compile(r'.*/sales/')

    def __init__(self, *args, **kwargs):
        super(TemplateSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//div[@class="price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="spacer"]/following-sibling::*//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@id="slides-pics"]/img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        item['bedrooms'] = response.xpath(
            '//div[@class="beds"]/text()').extract_first()
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="view"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
