# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CosmopropSpider(scrapy.Spider):
    name = "cosmoprop"
    allowed_domains = ["www.cosmoprop.co.uk"]

    start_urls = [
        'http://www.cosmoprop.co.uk/search?tratyp=Resale&onlyref=&location=&minprice=&maxprice=&let_minprice=&let_maxprice=&minbedrooms=&maxbedrooms=&branch=7841&orderby=&incstc=&rows_per_page=12&csearch=&pageno=1',
        'http://www.cosmoprop.co.uk/search?tratyp=Letting&onlyref=&location=&minprice=&maxprice=&let_minprice=0&let_maxprice=1000&minbedrooms=&maxbedrooms=&branch=7841&orderby=&incstc=&rows_per_page=12&csearch=&pageno=1'
    ]

    REGEX_FOR_RENT = re.compile(r'.*tratyp=Letting')
    REGEX_FOR_SALE = re.compile(r'.*tratyp=Resale')

    def __init__(self, *args, **kwargs):
        super(CosmopropSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//div[@class="head-price"]/h2/strong/text()').extract_first()
        item['description'] = response.xpath('//div[@class="property-overview property-desc"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="cycle-slideshow"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//i[@class="icon icon-bedrooms"]/following-sibling::text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//i[@class="icon icon-bathrooms"]/following-sibling::text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//a[@title="View Property"]/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//div[@class="pagination"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
