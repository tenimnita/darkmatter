# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CoultonsSpider(scrapy.Spider):
    name = "coultons"
    allowed_domains = ["www.coultons.co.uk"]

    start_urls = [
        'http://www.coultons.co.uk/search-property-result/',
    ]

    def __init__(self, *args, **kwargs):
        super(CoultonsSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath(
            '//span[@class="col-sm-7 detail-field-value price-value"]//text()').extract()
        item['description'] = response.xpath('//div[@class="property-content"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="thumbnails-wrap"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//span[@class="value-_bedrooms col-sm-7 detail-field-value"]/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//span[@class="value-_bathrooms col-sm-7 detail-field-value"]/text()').extract_first()
        item['square'] = response.xpath(
            '//span[@class="value-_area col-sm-7 detail-field-value"]/text()').extract_first()
        item['type'] = response.xpath(
            '//span[@class="col-sm-7 detail-field-value status-value"]/a/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//h2[@class="property-title"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//div[@class="pagination list-center"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
