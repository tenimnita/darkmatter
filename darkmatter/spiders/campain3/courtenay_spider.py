# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CourtenaySpider(scrapy.Spider):
    name = "courtenay"
    allowed_domains = ["www.courtenay.co.uk"]

    start_urls = [
        'http://www.courtenay.co.uk/search.aspx?ListingType=5&igid=&imgid=9&egid=&emgid=&category=1&defaultlistingtype=5&markettype=0&cur=GBP',
        'http://www.courtenay.co.uk/search.aspx?ListingType=6&igid=&imgid=9&egid=&emgid=&category=1&defaultlistingtype=5&markettype=0&cur=GBP'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/for-rent/')
    REGEX_FOR_SALE = re.compile(r'.*/for-sale/')

    def __init__(self, *args, **kwargs):
        super(CourtenaySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def change_img_resolution(self, img_url):
        matched = re.match(r'(.*)&h=(\d+)&w=(\d+)(.*)', img_url)
        if matched:
            return u'{}&h=397&w=597{}'.format(matched.group(1), matched.group(4))
        return img_url

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/span/text()').extract_first()
        item['price'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblFormattedPrice"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="FDContent"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//table[@class="FDSmallImage"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [self.change_img_resolution(url) for url in image_urls]
        item['bedrooms'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_txtBedrooms"]/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_txtBathrooms"]/text()').extract_first()
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        # add next item
        for url in response.xpath('//a[@class="LandlordsLink"]/@href').extract():
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="ListResultContainerLeft"]/h1/div/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
            # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
            #     paging_url = urljoin(base=response.url, url=paging_url)
            #     yield Request(url=paging_url, callback=self.parse)
