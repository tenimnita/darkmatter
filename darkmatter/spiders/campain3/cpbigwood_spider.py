# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CpbigwoodSpider(scrapy.Spider):
    name = "cpbigwood"
    allowed_domains = ["www.cpbigwood.com"]

    start_urls = [
        'http://www.cpbigwood.com/property/commercial/search?PropertySearchFormsCommercials%5BpropertyCategory%5D=1&PropertySearchFormsCommercials%5BpropertyType%5D=&PropertySearchFormsCommercials%5Bquery%5D=&PropertySearchFormsCommercials%5Bradius%5D=&PropertySearchFormsCommercials%5BsalePriceRangeMin%5D=&PropertySearchFormsCommercials%5BsalePriceRangeMax%5D=&PropertySearchFormsCommercials%5BletPriceRangeMin%5D=&PropertySearchFormsCommercials%5BletPriceRangeMax%5D=&PropertySearchFormsCommercials%5Bsquare%5D=1&PropertySearchFormsCommercials%5BSizeMin%5D=&PropertySearchFormsCommercials%5BSizeMax%5D=&yt0=',
        'http://www.cpbigwood.com/property/commercial/search?PropertySearchFormsCommercials%5BpropertyCategory%5D=2&PropertySearchFormsCommercials%5BpropertyType%5D=&PropertySearchFormsCommercials%5Bquery%5D=&PropertySearchFormsCommercials%5Bradius%5D=&PropertySearchFormsCommercials%5BsalePriceRangeMin%5D=&PropertySearchFormsCommercials%5BsalePriceRangeMax%5D=&PropertySearchFormsCommercials%5BletPriceRangeMin%5D=&PropertySearchFormsCommercials%5BletPriceRangeMax%5D=&PropertySearchFormsCommercials%5Bsquare%5D=1&PropertySearchFormsCommercials%5BSizeMin%5D=&PropertySearchFormsCommercials%5BSizeMax%5D=&yt0=',
    ]

    REGEX_FOR_SALE = re.compile(r'.*propertyCategory%5D=1')
    REGEX_FOR_RENT = re.compile(r'.*propertyCategory%5D=2')

    def __init__(self, *args, **kwargs):
        super(CpbigwoodSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//p[@class="sectionUnderTitlePrice"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="contentSection auctionView"]/div[3]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@id="cpCurrentImg"]/img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="buttonsBox"]/a[1]/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//ul[@class="yiiPager"]/li/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
