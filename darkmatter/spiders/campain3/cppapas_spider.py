# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CppapasSpider(scrapy.Spider):
    name = "cppapas"
    allowed_domains = ["www.cppapas.co.uk"]

    start_urls = [
        'http://www.cppapas.co.uk/let.php',
        'http://www.cppapas.co.uk/sale.php'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/let.php')
    REGEX_FOR_SALE = re.compile(r'.*/sale.php')

    def __init__(self, *args, **kwargs):
        super(CppapasSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//li[@class="property-details-price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="small-9 columns hard--left"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@class="example-orbit"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="properties-holder"]//a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
            # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
            #     paging_url = urljoin(base=response.url, url=paging_url)
            #     yield Request(url=paging_url, callback=self.parse)
