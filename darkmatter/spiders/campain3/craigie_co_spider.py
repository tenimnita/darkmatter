# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CraigiecoSpider(scrapy.Spider):
    name = "craigie_co"
    allowed_domains = ["www.craigie-co.co.uk"]

    start_urls = [
        'http://www.craigie-co.co.uk/property/?post_type=property&property_type=&status=for-sale&beds=&baths=&min-price=&max-price=aths=&min-price=&max-price=',
        'http://www.craigie-co.co.uk/property/?post_type=property&property_type=&status=for-rent&beds=&baths=&min-price=&max-price=',
        'http://www.craigie-co.co.uk/property/?post_type=property&property_type=&status=Let&beds=&baths=&min-price=&max-price='
    ]

    def __init__(self, *args, **kwargs):
        super(CraigiecoSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2[@class="prop-title pull-left margin0"]/text()').extract_first()
        item['price'] = response.xpath('//span[@class="prop-price pull-right serif italic"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="clearfix padding030"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="cycle-slideshow"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//span[text()="Bedrooms"]/following-sibling::span/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//span[text()="Bathrooms"]/following-sibling::span/text()').extract_first()
        item['type'] = response.xpath('//span[@class="absoluteinfo prop-tag"]/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="holder row"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
