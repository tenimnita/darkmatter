# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CrayandnortonSpider(scrapy.Spider):
    name = "crayandnorton"
    allowed_domains = ["www.crayandnorton.co.uk"]

    start_urls = [
        'http://www.crayandnorton.co.uk/results.asp?market=1&searchurl=%2Fdefault%2Easp%3F&view=grid&pricetype=1&search=Search',
        'http://www.crayandnorton.co.uk/results.asp?market=1&searchurl=%2Fdefault%2Easp%3F&view=grid&pricetype=3&search=Search&'
    ]

    REGEX_FOR_RENT = re.compile(r'.*pricetype=3')
    REGEX_FOR_SALE = re.compile(r'.*pricetype=1')

    def __init__(self, *args, **kwargs):
        super(CrayandnortonSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        # item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//div[@id="detail-header"]/h1/text()').extract_first()
        item['description'] = response.xpath('//div[@id="detail-content"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@id="gallery-2"]/a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//span[@class="bedrooms"]/span/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//span[@class="bathrooms"]/span/text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        for item_node in response.xpath('//div[contains(@class, "results1_propertyborder_grid")]'):
            url = item_node.xpath('.//div[@class="res-prop-lnk"]/a/@href').extract_first()
            url = urljoin(response.url, url)
            title = item_node.xpath('./div[@class="header2"]/h2/text()').extract_first()
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=type,
                title=title
            )))
        # urls = response.xpath('//div[@class="res-prop-lnk"]/a/@href').extract()
        # if len(urls) == 0:
        #     self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        # for url in urls:
        #     url = urljoin(response.url, url)
        #     yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//div[@class="results-pagination"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
