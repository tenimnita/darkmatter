# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CraysonSpider(scrapy.Spider):
    name = "crayson"
    allowed_domains = ["www.crayson.com"]

    start_urls = [
        'http://www.crayson.com/search-results/',
    ]

    def __init__(self, *args, **kwargs):
        super(CraysonSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@class="sin_address"]/text()').extract_first()
        item['price'] = response.xpath('//div[@class="sin_price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="sin_description"]//text()').extract()
        # Parse image urls
        # image_urls = response.xpath('//ul[@class="slides"]//img/@src').extract()
        # image_urls = list(set(image_urls))
        # item['image_urls'] = image_urls
        for text in response.xpath('//div[@class="sin_itemsdetail"]/ul/li/text()').extract():
            matched = re.match(r'Bedroom.*(\d+)', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
            matched = re.match(r'Bathrooms.*(\d+)', text, re.IGNORECASE)
            if matched:
                item['bathrooms'] = matched.group(1)
        item['type'] = 'sale'
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="description"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
