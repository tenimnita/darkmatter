# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CredentialSpider(scrapy.Spider):
    name = "credential"
    allowed_domains = ["www.credential.co.uk"]

    start_urls = [
        'http://www.credential.co.uk/properties/?category=sale&bedrooms=0&min_price=50000&max_price=2000000',
        'http://www.credential.co.uk/properties/?category=let&bedrooms=0&min_price=50000&max_price=2000000'
    ]

    REGEX_FOR_RENT = re.compile(r'.*category=let')
    REGEX_FOR_SALE = re.compile(r'.*category=sale')

    def __init__(self, *args, **kwargs):
        super(CredentialSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//*[@id="property"]/div[2]/div[1]/div[1]/h2/text()').extract_first()
        item['price'] = response.xpath('//p[@class="price"]//text()').extract()
        item['description'] = response.xpath('//div[@id="details"]/div[1]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@id="images"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        for item_node in response.xpath('//div[@class="property"]'):
            url = item_node.xpath('.//div[@class="property-text"]/h3/a/@href').extract_first()
            url = urljoin(response.url, url)
            text = item_node.xpath('.//p[@class="details"]/text()').extract_first()
            matched = re.search(r'(\d+)\s+bedroom', text, re.IGNORECASE)
            if matched:
                bedrooms = matched.group(1)
            matched = re.search(r'(\d+)\s+bathroom', text, re.IGNORECASE)
            if matched:
                bathrooms = matched.group(1)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                bedrooms=bedrooms,
                bathrooms=bathrooms,
                type=type
            )))
        for paging_url in response.xpath('//div[@id="pagination"]/ul/li/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
