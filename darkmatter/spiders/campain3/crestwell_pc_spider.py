# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CrestwellPcSpider(scrapy.Spider):
    name = "crestwell_pc"
    allowed_domains = ["www.crestwell-pc.com"]

    start_urls = [
        'http://www.crestwell-pc.com/let/property-to-let/',
        'http://www.crestwell-pc.com/buy/property-for-sale/'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/let/')
    REGEX_FOR_SALE = re.compile(r'.*/buy/')

    def __init__(self, *args, **kwargs):
        super(CrestwellPcSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//span[@class="details_price"]//text()').extract()
        item['description'] = response.xpath('//div[@class="propertyInfo_desc propertyInfo_box"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="tn3 album"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        text = response.xpath('//h2[@class="details_h2"]/text()[2]').extract_first()
        matched = re.search(r'(\d+)\s+bedroom', text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        matched = re.search(r'(\d+)\s+bathroom', text, re.IGNORECASE)
        if matched:
            item['bathrooms'] = matched.group(1)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="results-gallery-image"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
