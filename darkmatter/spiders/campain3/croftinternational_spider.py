# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CroftinternationalSpider(scrapy.Spider):
    name = "croftinternational"
    allowed_domains = ["www.croftinternational.com"]

    start_urls = [
        'http://www.croftinternational.com/index.php?v=londona',
        'http://www.croftinternational.com/index.php?v=dubaia',
        'http://www.croftinternational.com/index.php?v=southoffrancea',
        'http://www.croftinternational.com/index.php?v=rasalkhaimaha',
    ]

    REGEX_FOR_RENT = re.compile(r'.*v=Rent')
    REGEX_FOR_SALE = re.compile(r'.*v=Buy')

    def __init__(self, *args, **kwargs):
        super(CroftinternationalSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//span[@itemprop="addressLocality"]/text()').extract_first()
        item['description'] = response.xpath('//td[@class="descriptiontext"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//a[@class="imglisting"]/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))

        for item_node in response.xpath(
                '//*[@id="bodycontents1"]/div/table/tr[3]/td/table//div[@class="notmobileversion"]'):
            url = item_node.xpath('.//a/@href').extract_first()
            price = item_node.xpath('.//td[@class="featprice"]/text()').extract_first()
            bedrooms = item_node.xpath(
                './/img[@src="http://www.croftinternational.com/data/bed.png"]/preceding-sibling::text()').extract_first()
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                bedrooms=bedrooms,
                price=price
            )))
