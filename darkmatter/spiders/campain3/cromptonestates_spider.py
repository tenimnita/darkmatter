# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CromptonestatesSpider(scrapy.Spider):
    name = "cromptonestates"
    allowed_domains = ["www.zoopla.co.uk"]

    start_urls = [
        'http://www.zoopla.co.uk/to-rent/branch/crompton-estates-london-35984/',
        'http://www.zoopla.co.uk/for-sale/branch/crompton-estates-london-35984/'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/to-rent/')
    REGEX_FOR_SALE = re.compile(r'.*/for-sale/')

    def __init__(self, *args, **kwargs):
        super(CromptonestatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2[@itemprop="streetAddress"]/text()').extract_first()
        item['price'] = response.xpath('//div[@class="listing-details-price text-price"]//text()').extract()
        item['description'] = response.xpath('//div[@itemprop="description"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@id="images-main"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//span[@class="num-icon num-beds"]/@title').extract_first()
        item['bathrooms'] = response.xpath(
            '//span[@class="num-icon num-baths"]/@title').extract_first()
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="listing-results-right clearfix"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
