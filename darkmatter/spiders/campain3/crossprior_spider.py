# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CrosspriorSpider(scrapy.Spider):
    name = "crossprior"
    allowed_domains = ["www.crossprior.co.uk"]

    start_urls = [
        'http://www.crossprior.co.uk/search.aspx?ListingType=5&areainformation=&areainformationname=Location&igid=&imgid=&egid=&emgid=&category=1&defaultlistingtype=5&markettype=0&cur=GBP',
        'http://www.crossprior.co.uk/search.aspx?ListingType=6&areainformation=&areainformationname=Location&igid=&imgid=&egid=&emgid=&category=1&defaultlistingtype=5&markettype=0&cur=GBP'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/for-rent/')
    REGEX_FOR_SALE = re.compile(r'.*/for-sale/')

    def __init__(self, *args, **kwargs):
        super(CrosspriorSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def resize_img_url(self, url):
        matched = re.match(r'(.*)&h=83&w=125(.*)', url)
        if matched:
            return u'{}&h={}&w={}{}'.format(matched.group(1), 1050, 1680, matched.group(2))

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath(
            '//h1[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblFormattedAddress"]/text()').extract_first()
        item['price'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblFormattedPrice"]/text()').extract_first()
        item['description'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblBriefDescription"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="extraPhotosHolder"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [self.resize_img_url(url) for url in image_urls]
        text = response.xpath('//h3[@class="FdBedroomType"]/span/text()').extract_first()
        matched = re.search(r'(\d+)\sbedroom', text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        next_item_url = response.xpath(
            '//a[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_cntrlNextPrevious_lnkNext"]/@href').extract_first()
        if next_item_url:
            next_item_url = urljoin(response.url, next_item_url)
            yield Request(url=next_item_url, callback=self.parse_item)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="propertyHolder"]//h2/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
