# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CrownlinepropertySpider(scrapy.Spider):
    name = "crownlineproperty"
    allowed_domains = ["www.crownlineproperty.co.uk"]

    start_urls = [
        'http://www.crownlineproperty.co.uk/properties/4585143887',
    ]

    def __init__(self, *args, **kwargs):
        super(CrownlinepropertySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@class="page_shop-page_15 product-detail"]//h1/text()').extract()
        item['price'] = response.xpath('//div[@class="price body"]//text()').extract()
        item['description'] = response.xpath('//div[@class="page_shop-page_15_3 body"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="page_shop-page_15_11"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//li[@class="page_properties_19_18"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
