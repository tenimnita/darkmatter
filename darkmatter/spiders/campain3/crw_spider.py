# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CrwSpider(scrapy.Spider):
    name = "crw"
    allowed_domains = ["www.crw.uk.com"]

    start_urls = [
        'http://www.crw.uk.com/SearchResults.aspx?SearchData=v7|50|0|100000|0|99999998|0|0|3|3|1|1|1|Dagenham|54850:18450:50:_|2|2|0|2|1||2||||426|0:2147483647:0|0|1|2|::',
        'http://www.crw.uk.com/SearchResults.aspx?SearchData=v7|50|0|100000|0|99999998|0|0|3|3|1|1|1|Dagenham|54850:18450:50:_|2|2|0|2|1||2||||426|0:2147483647:0|0|2|2|::',
        'http://www.crw.uk.com/SearchResults.aspx?SearchData=v7|50|0|100000|0|99999998|0|0|3|3|1|1|1|RM8|54775:18639:50:_|2|8|0|2|1||2||||426|0:2147483647:0|0|1|2|::',
        'http://www.crw.uk.com/SearchResults.aspx?SearchData=v7|50|0|100000|0|99999998|0|0|3|3|1|1|1|RM8|54775:18639:50:_|2|8|0|2|1||2||||426|0:2147483647:0|0|2|2|::',
    ]

    def __init__(self, *args, **kwargs):
        super(CrwSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//a[@title="Property Details"]//text()').extract_first()
        item['price'] = response.xpath('//span[@class="price"]/text()').extract_first()
        item['description'] = response.xpath('//h4[text()="Description"]/following-sibling::p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@id="property_details_gallery"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        text = response.xpath('//div[@id="property_info"]/p/text()').extract_first()
        matched = re.search(r'(\d+)\s+bed', text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        matched = re.search(r'(\d+)\s+bath', text, re.IGNORECASE)
        if matched:
            item['bathrooms'] = matched.group(1)
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))

        type_text = response.xpath('//fieldset[@id="searchDetails"]/h2/text()').extract_first()
        if type_text:
            matched = re.search(r'Sale', type_text, re.IGNORECASE)
            if matched:
                type = 'sale'
            else:
                type = 'rent'

        urls = response.xpath('//div[@class="propertylist_content"]/h5/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            # print url
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//p[@class="right"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            # print paging_url
            yield Request(url=paging_url, callback=self.parse)
