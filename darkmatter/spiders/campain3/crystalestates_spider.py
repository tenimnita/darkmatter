# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CrystalestatesSpider(scrapy.Spider):
    name = "crystalestates"
    allowed_domains = ["www.crystalestates.co.uk"]

    start_urls = [
        'http://www.crystalestates.co.uk/properties.aspx?Mode=1&PriceMax=0&Bedrooms=0&Areas=&Commercial=0',
        'http://www.crystalestates.co.uk/properties.aspx?Mode=0&PriceMax=0&Bedrooms=0&Areas=&Commercial=0'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/property-to-rent')
    REGEX_FOR_SALE = re.compile(r'.*/property-for-sale')

    def __init__(self, *args, **kwargs):
        super(CrystalestatesSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//abbr[@class="geo"]/text()').extract_first()
        item['price'] = response.xpath('//div[@id="divMainPage"]/div[1]/div[2]/h1/span/text()').extract_first()
        item['description'] = response.xpath('//span[@id="ctl00_ContentPlaceHolderMain_lblFullDescription"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="tn3 album"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        text = response.xpath('//div[@id="divMainPage"]/div[1]/div[1]/h1/span/text()').extract_first()
        matched = re.search(r'(\d+)\s+bedroom', text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        if self.REGEX_FOR_RENT.match(response.url):
            item['type'] = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="propertylistimage"]//a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
