# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CsjSpider(scrapy.Spider):
    name = "csj_eu"
    allowed_domains = ["www.csj.eu.com"]

    start_urls = [
        'http://www.csj.eu.com/search_results.php?p_department=RS&propertyAddress=&officeID=&propertyType=&minimumPrice=&minimumRentFrequency=&minimumBedrooms=&maximumPrice=&maximumRentFrequency=',
        'http://www.csj.eu.com/search_results.php?p_department=RL&propertyAddress=&officeID=&propertyType=&minimumPrice=&minimumRentFrequency=pw&minimumBedrooms=&maximumPrice=&maximumRentFrequency=pw'
    ]

    REGEX_FOR_RENT = re.compile(r'.*p_department=RL')
    REGEX_FOR_SALE = re.compile(r'.*p_department=RS')

    def __init__(self, *args, **kwargs):
        super(CsjSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2[@class="title"]/text()').extract_first()
        item['price'] = response.xpath('//div[@class="details"]/text()[1]').extract_first()
        item['description'] = response.xpath('//div[@class="contentContainer innerContainer"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="slideshowContainer"]/img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        text = response.xpath('//div[@class="details"]/text()[2]').extract_first()
        matched = re.search(r'(\d+)\s+bedroom', text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        matched = re.search(r'(\d+)\s+bathroom', text, re.IGNORECASE)
        if matched:
            item['bathrooms'] = matched.group(1)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="propertyContainer "]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//div[@class="paging"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
