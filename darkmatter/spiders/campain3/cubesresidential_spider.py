# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CubesresidentialSpider(scrapy.Spider):
    name = "cubesresidential"
    allowed_domains = ["www.cubesresidential.com"]

    start_urls = [
        'http://www.cubesresidential.com/search/?instruction_type=Sale&showstc=on',
        'http://www.cubesresidential.com/search/?property_type=Commercial+Property&address_keyword=&showstc=on',
        'http://www.cubesresidential.com/search/?instruction_type=Letting&showstc=on'
    ]

    REGEX_FOR_RENT = re.compile(r'.*instruction_type=Sale')
    REGEX_FOR_SALE = re.compile(r'.*instruction_type=Letting')
    REGEX_FOR_COMMERCIAL = re.compile(r'.*property_type=Commercial')

    def __init__(self, *args, **kwargs):
        super(CubesresidentialSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@id="description-heading"]/text()').extract_first()
        item['price'] = response.xpath('//div[@id="price-heading"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="overview"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//ul[@id="pikachoose"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_COMMERCIAL.match(response.url):
            type = 'commercial'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="resultsThumbnail"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
