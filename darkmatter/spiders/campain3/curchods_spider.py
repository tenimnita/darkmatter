# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CurchodsSpider(scrapy.Spider):
    name = "curchods"
    allowed_domains = ["curchods.com"]

    start_urls = [
        'http://curchods.com/properties-for-sale-in/Surrey?field_property_department_value=RS',
        'http://curchods.com/lettings?field_property_department_value=RL',
    ]
    REGEX_FOR_RENT = re.compile(r'.*/properties-for-sale')
    REGEX_FOR_SALE = re.compile(r'.*/lettings')

    def __init__(self, *args, **kwargs):
        super(CurchodsSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//span[@class="city"]/text()').extract_first()
        item['price'] = response.xpath('//span[@class="city"]/span/text()').extract_first()
        item['description'] = response.xpath('//div[@id="property-sidebar-left"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//a[@class="property-gallery"]/img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        item['bedrooms'] = response.xpath(
            '//li[@class="bedrooms"]/span/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//li[@class="bathrooms"]/span/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        elif self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="property-result grid-result col-xs-12 col-sm-6"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//ul[@class="pager"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
