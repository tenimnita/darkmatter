# -*- coding: utf-8 -*-
import json
import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CurrellSpider(scrapy.Spider):
    name = "currell"
    allowed_domains = ["www.currell.com"]

    start_urls = [
        'http://www.currell.com/residential/search.json?type=1&areas=all&beds=0&from=0&to=2147483647&_=1474784067881',
        'http://www.currell.com/residential/search.json?type=3&areas=all&beds=0&from=0&to=2147483647&_=1474785773215',
        'http://www.currell.com/residential/search.json?type=2&areas=all&beds=0&from=0&to=2147483647&_=1474785804425',
        'http://www.currell.com/commercial/search.json?type=1&ptype=any&areas=all&afrom=0&ato=0&from=0&to=2147483647&_=1474786104079',
        'http://www.currell.com/commercial/search.json?type=3&ptype=any&areas=all&afrom=0&ato=0&from=0&to=2147483647&_=1474786146613',
    ]

    def __init__(self, *args, **kwargs):
        super(CurrellSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        # item['title'] = response.xpath('//h1/text()').extract_first()
        # item['price'] = response.xpath('//small[@class="eapow-detail-price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="editor"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//*[@class="rsImg"]/@data-rstmb').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        json_item = json.loads(response.body)
        if json_item:
            for item in json_item['all']:
                item = DarkmatterItem(
                    url=urljoin(response.url, item['url']),
                    title=item['name'],
                    bedrooms=item['beds'],
                    bathrooms=item['bathrooms'],
                    price=item['price_label'],
                    description=item['description'],
                    type='sale' if item['category'] == 1 else 'rent'
                )
                yield Request(url=item['url'], callback=partial(self.parse_item, item))

                # urls = response.xpath('//div[@class="eapow-overview-short-desc"]//a/@href').extract()
                # if len(urls) == 0:
                #     self.logger.warning('Cannot extract URL from: {}'.format(response.url))
                # for url in urls:
                #     url = urljoin(response.url, url)
                #     yield Request(url=url, callback=self.parse_item)
                # for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
                #     paging_url = urljoin(base=response.url, url=paging_url)
                #     yield Request(url=paging_url, callback=self.parse)
