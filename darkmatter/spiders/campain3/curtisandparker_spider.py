# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CurtisandparkerSpider(scrapy.Spider):
    name = "curtisandparker"
    allowed_domains = ["www.curtisandparker.co.uk"]

    start_urls = [
        'http://www.curtisandparker.co.uk/search/?showstc=on&short_let=0&instruction_type=Sale&address_keyword=&property_type=&bedrooms=0&minpricew=&maxpricew=&Search2=Search',
        'http://www.curtisandparker.co.uk/search/?showstc=on&short_let=0&instruction_type=Letting&address_keyword=&property_type=&bedrooms=0&minpricew=&maxpricew=&Search2=Search',
    ]

    REGEX_FOR_RENT = re.compile(r'.*instruction_type=Letting')
    REGEX_FOR_SALE = re.compile(r'.*instruction_type=Sale')

    def __init__(self, *args, **kwargs):
        super(CurtisandparkerSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):

        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//h1/em/text()').extract_first()
        item['description'] = response.xpath('//h2[text()="Property Details"]/following-sibling::p//text()').extract()
        # Parse image urls
        # image_urls = response.xpath('//div[@id="prop-thumbs"]/img/@src').extract()
        # image_urls = list(set(image_urls))
        # item['image_urls'] = image_urls
        # item['bedrooms'] = response.xpath(
        #     '//img[@src="/images/bedrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        # item['bathrooms'] = response.xpath(
        #     '//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        for item_node in response.xpath('//div[@class="property"]'):
            url = item_node.xpath('./a/@href').extract_first()
            url = urljoin(response.url, url)
            bedrooms = item_node.xpath('.//p[@class="marg"]/text()[1]').extract_first()
            bathrooms = item_node.xpath('.//p[@class="marg"]/text()[3]').extract_first()
            imgs = item_node.xpath('./div[@class="prop-image"]//img/@src').extract()
            imgs = [urljoin(response.url, url) for ull in imgs]
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=type,
                bedrooms=bedrooms,
                bathrooms=bathrooms,
                image_urls=imgs
            )))
        for paging_url in response.xpath('//p[@class="pagi"]/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
