# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CurtisresidentialSpider(scrapy.Spider):
    name = "curtisresidential"
    allowed_domains = ["www.curtisresidential.co.uk"]

    start_urls = [
        'http://www.curtisresidential.co.uk/property-search/',
    ]

    def __init__(self, *args, **kwargs):
        super(CurtisresidentialSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@id="single-property-title"]//h1/text()').extract_first()
        item['price'] = response.xpath('//div[@id="single-property-title"]/p/text()[2]').extract_first()
        item['description'] = response.xpath('//div[@class="scroll-content"]/p//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="background-image cover-bg"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@id="list-properties"]//a/@href').extract()

        for item_node in response.xpath('//div[@id="list-properties"]/div'):
            url = item_node.xpath('.//a/@href').extract_first()
            bedrooms = item_node.xpath('.//ul[@class="clearfix list-property-info"]/li[1]//text()').extract()
            bathrooms = item_node.xpath('.//ul[@class="clearfix list-property-info"]/li[2]//text()').extract()
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                bedrooms=bedrooms,
                bathrooms=bathrooms
            )))
        # if len(urls) == 0:
        #     self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        # for url in urls:
        #     url = urljoin(response.url, url)
        #     yield Request(url=url, callback=self.parse_item)
        if len(urls) > 0:
            yield Request(url=self.next_page(response.url), callback=self.parse)

    def next_page(self, url):
        matched = re.match(r'(.*)/page/(\d+)', url)
        if matched:
            return u'{}/page/{}'.format(matched.group(1), int(matched.group(2)) + 1)
        else:
            return u'http://www.curtisresidential.co.uk/property-search/page/2/'
