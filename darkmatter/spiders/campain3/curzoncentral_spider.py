# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class CurzoncentralSpider(scrapy.Spider):
    name = "curzoncentral"
    allowed_domains = ["property.curzoncentral.com"]

    start_urls = [
        'http://property.curzoncentral.com/buy/property-for-sale/',
        'http://property.curzoncentral.com/let/property-to-let/'
    ]

    REGEX_FOR_RENT = re.compile(r'.*/let/')
    REGEX_FOR_SALE = re.compile(r'.*/buy/')

    def __init__(self, *args, **kwargs):
        super(CurzoncentralSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//span[@class="nativecurrencyvalue"]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="module-description"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="slickGalery "]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath(
            '//div[@class="details-stats"]/span[1]/text()').extract_first()
        item['bathrooms'] = response.xpath(
            '//div[@class="details-stats"]/span[2]/text()').extract_first()
        # item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="module-content"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(
                type=type
            )))
        for paging_url in response.xpath('//ul[@class="pagination"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
