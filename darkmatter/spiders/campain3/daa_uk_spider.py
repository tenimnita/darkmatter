# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class DaaukSpider(scrapy.Spider):
    name = "daa-uk"
    allowed_domains = ["www.daa-uk.com"]

    start_urls = [
        'http://www.daa-uk.com/search?mstq=1&location=&type=&beds_max=&beds_min=&sr=S&price_min=&price_max=&rent_min=&rent_max=&submit=Search',
        'http://www.daa-uk.com/search?mstq=1&location=&type=&beds_max=&beds_min=&sr=R&price_min=&price_max=&rent_min=&rent_max=&submit=Search'
    ]

    REGEX_FOR_RENT = re.compile(r'.*&sr=R')
    REGEX_FOR_SALE = re.compile(r'.*&sr=S')

    def __init__(self, *args, **kwargs):
        super(DaaukSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2/text()').extract_first()
        item['price'] = response.xpath('//div[@class="a-value"]/text()').extract_first()
        item['description'] = response.xpath(
            '//div[@class="ms_turbine single overide"]/div[@class="value"][1]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="additional-images"]/img/@rel').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        text = response.xpath('//div[@class="mst-topline"]/text()').extract_first()
        if text:
            matched = re.search(r'(\d+)\s+bed', text, re.IGNORECASE)
            if matched:
                item['bedrooms'] = matched.group(1)
            matched = re.search(r'(\d+)\s+Bath', text, re.IGNORECASE)
            if matched:
                item['bathrooms'] = matched.group(1)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="property"]/div/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
            # for paging_url in response.xpath('//div[@class="pagination"]/a/@href').extract():
            #     paging_url = urljoin(base=response.url, url=paging_url)
            #     yield Request(url=paging_url, callback=self.parse)
