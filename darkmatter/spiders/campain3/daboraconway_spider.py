# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class DaboraconwaySpider(scrapy.Spider):
    name = "daboraconway"
    allowed_domains = ["www.daboraconway.com"]

    start_urls = [
        'http://www.daboraconway.com/search/?showstc=on&showsold=on&price_fuzz_pc=10&instruction_type=Sale&bid=&max_bedrooms=&bedrooms=0&maxprice=&minprice=&Search.x=32&Search.y=19&Search=SEARCH&address_keyword=',
        'http://www.daboraconway.com/search/?showstc=on&showsold=on&price_fuzz_pc=10&instruction_type=Letting&bid=&max_bedrooms=&bedrooms=0&maxprice=&minprice=&Search.x=36&Search.y=15&Search=SEARCH&address_keyword='
    ]

    REGEX_FOR_RENT = re.compile(r'.*&instruction_type=Letting')
    REGEX_FOR_SALE = re.compile(r'.*&instruction_type=Sale')

    def __init__(self, *args, **kwargs):
        super(DaboraconwaySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        self.logger.info('parse item: {}'.format(response.url))
        # item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2/em/text()').extract_first()
        item['price'] = response.xpath('//h2/strong/text()').extract_first()
        item['description'] = response.xpath('//div[@class="property-detalis"]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@class="prop-image"]/img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = [urljoin(response.url, url) for url in image_urls]
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        type = None
        if self.REGEX_FOR_SALE.match(response.url):
            type = 'sale'
        elif self.REGEX_FOR_RENT.match(response.url):
            type = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        urls = response.xpath('//div[@class="prop-desc"]/p/a[1]/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(type=type)))
        for paging_url in response.xpath('//div[@class="pagination"]/p/a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
