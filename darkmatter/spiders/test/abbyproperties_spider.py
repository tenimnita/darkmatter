# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class AbbySpider(scrapy.Spider):
    name = "abbyproperties"
    allowed_domains = ["www.abbyproperties.com"]

    start_urls = [
        'http://www.abbyproperties.com/search.aspx?OrderByColumnIndex=0&Page=2&OrderByDirection=0&ItemsPerPage=1000&Currency=GBP',
    ]

    FOR_SALE_REGEX = re.compile(r'^.*/for-sale/.*$')
    FOR_RENT_REGEX = re.compile(r'^.*/for-rent/.*$')

    def __init__(self, *args, **kwargs):
        super(AbbySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract()
        item['price'] = response.xpath(
            '//span[@id="ctl00_cntrlCenterRegion_cntrlProperties_item_0_ctl00_lblFormattedPrice"]/text()').extract_first()
        # item['bedrooms'] = response.xpath('//span[@class="details-beds"]/text()').extract()
        item['description'] = response.xpath('//div[@class="PropertyDetails"]//text()').extract()
        item['image_urls'] = response.xpath('//div[@id="gallery-1"]//img/@src').extract()
        #
        if self.FOR_SALE_REGEX.match(response.url):
            item['type'] = 'sale'
        elif self.FOR_RENT_REGEX.match(response.url):
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)

        yield item

    def parse(self, response):
        for item_link in response.xpath('//div[@class="ListSearchResult panel"]'):
            url = item_link.xpath('div//a/@href').extract_first()
            if url:
                url = urljoin(response.url, url)
                yield Request(url=url, callback=self.parse_item)
            else:
                self.logger.warning('Cannot extract URL from: %s', item_link.extract())
