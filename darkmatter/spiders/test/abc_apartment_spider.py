# -*- coding: utf-8 -*-

import re
import urlparse
from functools import partial
from urlparse import urljoin

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class AbcApartmentSpider(scrapy.Spider):
    name = "abcapartment"
    allowed_domains = ["www.abc-apartments.com"]

    start_urls = [
        'http://www.abc-apartments.com/v2/index.php/properties-to-let?start=0',
        'http://www.abc-apartments.com/v2/index.php/short-let?start=0',
        'http://www.abc-apartments.com/v2/index.php/properties-for-sale?start=0'
    ]

    FOR_SALE_REGEX = re.compile(r'^.*/properties-for-sale\?.*$')
    FOR_RENT_REGEX = re.compile(r'^.*/properties-to-let\?.*$')
    FOR_RENT_REGEX_1 = re.compile(r'^.*/short-let\?.*$')

    def __init__(self, *args, **kwargs):
        super(AbcApartmentSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@class="ip_mainheader"]/h2/text()').extract_first()
        item['price'] = response.xpath('//span[@class="pe_price"]/text()').extract_first()
        item['bedrooms'] = response.xpath('//div[@class="ip_beds"]//text()').extract()
        item['bathrooms'] = response.xpath('//div[@class="ip_baths"]//text()').extract()

        address_node = response.xpath('//div[@class="ip_sidecol_mainaddress"]')
        if len(address_node) > 0:
            item['address'] = address_node[0].xpath('.//text()').extract()

        item['description'] = response.xpath('//*[@id="detailsPane"]/dd[2]//text()').extract()
        # Parse image urls
        image_urls = response.xpath('//div[@style="display: none !important;"]//a/@href').extract()
        image_from_tab = response.xpath('//div[@class="ip_imagetab"]/a/@href').extract_first()
        if image_from_tab:
            image_urls.append(image_from_tab)
        image_urls = [urlparse.urljoin(base=response.url, url=url) for url in image_urls]
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        # #
        if self.FOR_SALE_REGEX.match(response.url):
            item['type'] = 'sale'
        elif self.FOR_RENT_REGEX.match(response.url) or self.FOR_RENT_REGEX_1.match(response.url):
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)

        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="property_overview_title"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        next_page = response.xpath('//li[@class="pagination-next"]/a/@href').extract_first()
        print 'next page {}'.format(next_page)
        if next_page:
            url = urlparse.urljoin(base=response.url, url=next_page)
            print url
            yield Request(url=url, callback=self.parse)
