# -*- coding: utf-8 -*-

import re
import urlparse
from functools import partial
from urlparse import urljoin

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class AdmiralSpider(scrapy.Spider):
    name = "admiral-property"
    allowed_domains = ["www.admiral-property.co.uk"]

    start_urls = [
        'http://www.admiral-property.co.uk/results.asp?displayperpage=10&',
    ]

    def __init__(self, *args, **kwargs):
        super(AdmiralSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_type(self, response):
        if response.xpath('//span[@class="detail-propstat_sold"]').extract_first is not None:
            return 'sold'
        if response.xpath('//span[@class="detail-propstat_under_offer"]').extract_first is not None:
            return 'under_offer'
        if response.xpath('//span[@class="class="detail-propstat_for_sale""]').extract_first is not None:
            return 'sale'
        return None

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/a/text()').extract_first()
        item['price'] = response.xpath('//span[@class="priceask"]/text()').extract_first()
        item['bedrooms'] = response.xpath('//span[@class="bedroom"]/text()').extract_first()
        item['bathrooms'] = response.xpath('//span[@class="bathroom"]/text()').extract()
        item['description'] = response.xpath('//span[@class="available-date"]/following-sibling::p//text()').extract()
        image_urls = response.xpath('//div[@class="sp-slide"]/img/@src').extract()
        image_urls = list(set(image_urls))
        item['type'] = self.parse_type(response)

        item['image_urls'] = image_urls

        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="res-prop-lnk"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        next_page = response.xpath('//a[@title="Go to the next page of properties"]/@href').extract_first()
        if next_page:
            url = urlparse.urljoin(base=response.url, url=next_page)
            self.logger.info(u'next page: {}'.format(url))
            yield Request(url=url, callback=self.parse)
