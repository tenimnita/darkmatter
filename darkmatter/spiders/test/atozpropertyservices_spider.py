# -*- coding: utf-8 -*-

import re
import urlparse
from functools import partial
from urlparse import urljoin

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class AbcApartmentSpider(scrapy.Spider):
    name = "atozpropertyservices"
    allowed_domains = ["atozpropertyservices.co.uk"]

    start_urls = [
        'http://atozpropertyservices.co.uk/properties/',
    ]

    FOR_SALE_REGEX = re.compile(r'For Sale', re.IGNORECASE)
    FOR_RENT_REGEX = re.compile(r'For Rent', re.IGNORECASE)

    def __init__(self, *args, **kwargs):
        super(AbcApartmentSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['description'] = response.xpath('//div[@class="property-detail"]/p//text()').extract()

        # Parse image urls
        image_urls = response.xpath('//div[@class="preview"]/a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        # Parse others
        for node in response.xpath('//div[@class="pull-left overview"]//tr'):
            # print node.xpath('.').extract_first()
            th = node.xpath('./th/text()').extract_first()
            if 'Price:' in th:
                item['price'] = node.xpath('./td//text()').extract()
            elif 'Contract:' in th:
                type = node.xpath('./td/text()').extract_first()
                if self.FOR_RENT_REGEX.match(type):
                    item['type'] = 'rent'
                elif self.FOR_SALE_REGEX.match(type):
                    item['type'] = 'sale'
                else:
                    self.logger.warning('Cannot extract item tenure from: %s', response.url)
            elif 'Location' in th:
                item['address'] = node.xpath('./td/text()').extract_first()
            elif 'Bathrooms' in th:
                item['bathrooms'] = node.xpath('./td/text()').extract_first()
            elif 'Bedrooms' in th:
                item['bedrooms'] = node.xpath('./td/text()').extract_first()
            elif 'Area' in th:
                item['square'] = node.xpath('./td/text()').extract_first()

        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="properties-grid"]//a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for next_page in response.xpath('//div[@class="pagination pagination-centered"]//a/@href').extract():
            self.logger.info('next page {}'.format(next_page))
            url = urlparse.urljoin(base=response.url, url=next_page)
            yield Request(url=url, callback=self.parse)
