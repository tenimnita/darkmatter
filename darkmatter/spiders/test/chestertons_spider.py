# -*- coding: utf-8 -*-

import re
import urlparse
from functools import partial
from urlparse import urljoin

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class ChestertonsSpider(scrapy.Spider):
    name = "chestertons"
    allowed_domains = ["www.chestertons.com"]

    start_urls = [
        'http://www.chestertons.com/property-to-rent/search-results/',
        'http://www.chestertons.com/property-to-buy/search-results/'
    ]

    FOR_SALE_REGEX = re.compile(r'http://www\.chestertons\.com/property-to-rent/.*', re.IGNORECASE)
    FOR_RENT_REGEX = re.compile(r'http://www\.chestertons\.com/property-to-buy/.*', re.IGNORECASE)

    def __init__(self, *args, **kwargs):
        super(ChestertonsSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h2[@itemprop="address"]//text()').extract()
        item['description'] = response.xpath('//div[@class="content-block-left"]//p//text()').extract()
        item['price'] = response.xpath('//span[@class="price-conversion"]/text()').extract_first()
        # Parse image urls
        image_urls = response.xpath('//ul[@id="imageList"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        #
        item['bedrooms'] = response.xpath('//span[@class="icon-beds"]/following-sibling::text()').extract_first()
        item['bathrooms'] = response.xpath('//span[@class="icon-baths"]/following-sibling::text()').extract_first()

        if self.FOR_RENT_REGEX.match(response.url):
            item['type'] = 'rent'
        elif self.FOR_SALE_REGEX.match(response.url):
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="property-details"]/h3/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for next_page in response.xpath('//div[@class="navigation"]/a/@href').extract():
            self.logger.info('next page {}'.format(next_page))
            url = urlparse.urljoin(base=response.url, url=next_page)
            yield Request(url=url, callback=self.parse)
