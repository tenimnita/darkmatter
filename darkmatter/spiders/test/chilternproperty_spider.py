# -*- coding: utf-8 -*-

import re
import urlparse
from functools import partial
from urlparse import urljoin

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class ChilternPropertySpider(scrapy.Spider):
    name = "chilternproperty"
    allowed_domains = ["chilternproperty.com"]

    start_urls = [
        'http://chilternproperty.com/listings.php?sale=2&order_by=price%20desc&list=1',
        'http://chilternproperty.com/listings.php?sale=1&order_by=price%20desc&list=1'
    ]

    def __init__(self, *args, **kwargs):
        super(ChilternPropertySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//div[@class="display_address"]/text()').extract_first()
        item['price'] = response.xpath('//div[@class="display_price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="details_description_wrapper"]//text()').extract()

        # Parse image urls
        image_urls = response.xpath('//ul[@class="slides"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls

        # Parse others
        for information in response.xpath('//div[@id="details_info_wrapper"]//div[@class="button"]/p/text()').extract():
            # print node.xpath('.').extract_first()
            if 'Bedroom' in information:
                item['bedrooms'] = information
            elif 'Bathroom' in information:
                item['bathrooms'] = information

        type_text = response.xpath('//div[@class="title_subtitle_holder_inner"]//h1/span/text()').extract_first()
        if 'Let' in type_text:
            item['type'] = 'let'
        elif 'Sale' in type_text:
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)

        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@id="summary_wrapper"]//p[@class="display_address"]/a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for next_page in response.xpath('//div[@class="pagin"]//a/@href').extract():
            # self.logger.info('paging page {}'.format(next_page))
            url = urlparse.urljoin(base=response.url, url=next_page)
            yield Request(url=url, callback=self.parse)
