# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class FirstChoiceSpider(scrapy.Spider):
    name = "1stchoiceestates"
    allowed_domains = ["www.1stchoiceestates.com"]

    start_urls = [
        'http://www.1stchoiceestates.com/property-list/properties-for-rent/',
        'http://www.1stchoiceestates.com/property-list/sales/',
    ]

    FOR_SALE_REGEX = re.compile(r'^.*/sale/.*$')
    FOR_RENT_REGEX = re.compile(r'^.*/rent/.*$')

    def __init__(self, *args, **kwargs):
        super(FirstChoiceSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        item['title'] = response.xpath('//h1/text()').extract()
        item['price'] = response.xpath('//*[@id="property-description"]/div[3]/p/a//text()').extract()
        item['bedrooms'] = response.xpath('//span[@class="details-beds"]/text()').extract()
        item['bathrooms'] = response.xpath('//span[@class="details-baths"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="description"]//text()').extract()
        item['square'] = response.xpath('//span[@class="details-size"]/text()').extract_first()
        item['image_urls'] = response.xpath('//div[@id="ts-gallery"]//a/@href').extract()
        #
        if self.FOR_SALE_REGEX.match(response.url):
            item['type'] = 'sale'
        elif self.FOR_RENT_REGEX.match(response.url):
            item['type'] = 'rent'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)

        price_text = '\n'.join(item['price'])
        if 'Per Week' in price_text:
            item['price_type'] = 'week'
        elif 'Per Calendar Month' in price_text:
            item['price_type'] = 'month'
        else:
            self.logger.warning('Unknown price type for the estate: %s', item['url'])
        if len(item['price']) > 0:
            item['price'] = item['price'][0].strip()
        yield item

    def parse(self, response):
        for item_link in response.css('#main > div.box-wrap.clearfix a'):
            url = item_link.css('a::attr(href)').extract()

            if url:
                url = urljoin(response.url, item_link.css('a::attr(href)').extract_first())
                yield Request(url=url, callback=partial(self.parse_item, DarkmatterItem(url=url)))
            else:
                self.logger.warning('Cannot extract URL from: %s', item_link.extract())
