# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class ChilternPropertySpider(scrapy.Spider):
    name = "madisonbrook"
    allowed_domains = ["madisonbrook.com"]

    start_urls = [
        'http://madisonbrook.com/Properties.html?Location=&CategoryID=1&MinBeds=0&Type=&MinPrice=0&MaxPrice=10000000&Page=1&PageSize=16&Order=1&International=no',
        'http://madisonbrook.com/Properties.html?Location=&CategoryID=2&MinBeds=0&Type=&MinPrice=0&MaxPrice=10000000&Page=1&PageSize=16&Order=1&International=no',
        'http://madisonbrook.com/Properties.html?Location=&CategoryID=3&MinBeds=0&Type=&MinPrice=0&MaxPrice=10000000&Page=1&PageSize=16&Order=1&International=no',
    ]

    def __init__(self, *args, **kwargs):
        super(ChilternPropertySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h3[@class="h75 hspaceb"]/text()').extract_first()
        item['price'] = response.xpath('//span[@class="price-box"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="cols2"]/p//text()').extract()

        # Parse image urls
        image_urls = response.xpath(
            '//div[@class="cols2"]/a[@id="ctl00_ContentPlaceHolder1_lnkPropertyImage"]/@href').extract()
        image_urls.extend(response.xpath('//div[@id="ctl00_ContentPlaceHolder1_pnlGallery"]//a/@href').extract())
        image_urls = [urljoin(base=response.url, url=img_url) for img_url in image_urls]
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        #
        item['bedrooms'] = response.xpath('//span[@id="ctl00_ContentPlaceHolder1_liBed"]/text()').extract_first()
        item['bedrooms'] = response.xpath('//span[@id="ctl00_ContentPlaceHolder1_liBath"]/text()').extract_first()
        #
        type_text = response.xpath('//h2[@class="h50 hspaceb hline"]/text()').extract_first().lower()
        if 'rental' in type_text:
            item['type'] = 'rent'
        elif 'sale' in type_text:
            item['type'] = 'sale'
        else:
            self.logger.warning('Cannot extract item tenure from: %s', response.url)

        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="box"]//a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            parse = urlparse(url)
            removed_query_url = parse._replace(query='', fragment='')
            yield Request(url=removed_query_url.geturl(), callback=self.parse_item)
        for paging_url in self.parse_paging(response):
            pass
            # print paging_url
            yield Request(url=paging_url, callback=self.parse)

    def parse_paging(self, response):
        # print response.url
        matched = re.search('(.*)&Page=(\d+)&(.*)', response.url, re.IGNORECASE)
        if matched:
            for page_number in response.xpath('//div[@id="subnav-bottom"]//a/text()').extract():
                yield u'{}&Page={}&{}'.format(matched.group(1), page_number, matched.group(3))
        else:
            self.logger.warning('no matched paging url')
