# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class ChilternPropertySpider(scrapy.Spider):
    name = "raven-homes"
    allowed_domains = ["www.raven-homes.com"]

    start_urls = [
        'http://www.raven-homes.com/properties.aspx?Mode=0&PriceMax=0&Bedrooms=0&Areas=',
        'http://www.raven-homes.com/properties.aspx?Mode=1&PriceMax=0&Bedrooms=0&Areas='
    ]

    def __init__(self, *args, **kwargs):
        super(ChilternPropertySpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//abbr[@class="geo"]/text()').extract_first()
        item['price'] = response.xpath('//*[@id="divMainPage"]/div[1]/div[2]/h1/span/text()').extract_first()
        item['description'] = response.xpath('//div[@class="descriptionRight"]//text()').extract()
        # # Parse image urls
        image_urls = response.xpath(
            '//div[@class="gallery-example-wrap"]//a/@href').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        # #
        h1_text = response.xpath('//h1//text()').extract_first()
        matched = re.match('(\d+) Bedroom', h1_text, re.IGNORECASE)
        if matched:
            item['bedrooms'] = matched.group(1)
        matched = re.search('For (Sale|Rent)', h1_text, re.IGNORECASE)
        if matched:
            item['type'] = matched.group(1)
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//a[@class="textlink"]/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            if url.startswith('contact.aspx'):
                continue
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
