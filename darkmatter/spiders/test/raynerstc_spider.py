# -*- coding: utf-8 -*-

import re
from functools import partial
from urlparse import urljoin, urlparse, parse_qs

import scrapy
from scrapy.http import Request

from darkmatter.items import DarkmatterItem


class RaynerstcSpider(scrapy.Spider):
    name = "raynerstc"
    allowed_domains = ["www.raynerstc.com"]

    start_urls = [
        'http://www.raynerstc.com/properties?eapowquicksearch=1&limitstart=0',
    ]

    def __init__(self, *args, **kwargs):
        super(RaynerstcSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, response):
        self.logger.info('parse item: {}'.format(response.url))
        item = DarkmatterItem()
        item['url'] = response.url
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['price'] = response.xpath('//small[@class="eapow-detail-price"]/text()').extract_first()
        item['description'] = response.xpath('//div[@class="span8 pull-left eapow-desc-wrapper"]//text()').extract()
        # # # Parse image urls
        image_urls = response.xpath('//ul[@class="slides"]//img/@src').extract()
        image_urls = list(set(image_urls))
        item['image_urls'] = image_urls
        item['bedrooms'] = response.xpath('//img[@src="/images/bedrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        item['bathrooms'] = response.xpath('//img[@src="/images/bathrooms.png"]/following-sibling::strong[1]/text()').extract_first()
        item['type'] = response.xpath('//div[@class="eapow-bannertopright"]/img/@alt').extract_first()
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        urls = response.xpath('//div[@class="eapow-overview-short-desc"]//a/@href').extract()
        if len(urls) == 0:
            self.logger.warning('Cannot extract URL from: {}'.format(response.url))
        for url in urls:
            url = urljoin(response.url, url)
            yield Request(url=url, callback=self.parse_item)
        for paging_url in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
            paging_url = urljoin(base=response.url, url=paging_url)
            yield Request(url=paging_url, callback=self.parse)
